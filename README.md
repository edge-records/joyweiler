# Table of Contents
[[_TOC_]]

# Joyweiler
Welcome!

This is my project to create a retro arcade Joystick which can be used with both period hardware such as the Commodore 64 or the Amiga 500, as well as with modern computers running Linux or MacOS. It also features an adjustable rapid fire.

![V4 back](img/mikelator_back.jpg)
![V4 top](img/mikelator_top.jpg)

## Joyweiler Versions

I've uploaded these different approaches:
* V2 - This one "outsources" the USB complexities to a 3rd party PCB that can be bought
* V3 - This one uses an ATtiny84A microcontroller to talk USB, which requires you to be able to flash this microcontroller chip with a firmware that can be built with code provided in this repo.
* V4 - This is just an upgrade of V3, offering 2 logical fire buttons.
* V5 - Another upgrade, introducing a 4x7 segment display, for showing the FIRE1 frequency in Hz, and for configuring various settings.

See also the [Version History](#version-history) section

## Design Considerations
Feel free to skip the following section if you just want to build the thing. You won't miss anything important. I'm mostly writing all of this to remind my future self of my decisions.

* **All in one, instead of the dongle life** - First of all it was clear to me that I wouldn't want to build 2 separate Joysticks, one for period, one for modern hardware, since the expensive bit of it isn't the electronics, but the Sanwa joystick and buttons, and the case. I could have built a pure period Joystick, and just built an adapter like the [hexagons](https://web.archive.org/web/20180428224227/http://www.hexagons.de:80/index.php/USB_Joystickadapter) adapter. But I thought it's neater if it's all in one case, no dongle life.
* **No fix cables** - I'm using jacks instead of permanently attached cables, since one of the two cables would always dangle around, since only one cable is needed at a time. This means more cost and work, to get jacks in, but I considered it worth the effort.
* **2 fire buttons** - A classic period Joystick would have one button only. Some of them have two mounted, but they do logically the same thing. There are lots of enhancements out there to make the second button optionally perform other functions. Some will put the "up" direction of the joystick onto the second button, others will redirect the space key to it, or implement the [Cheetah Annihilator](https://www.c64-wiki.de/wiki/Cheetah_Annihilator) setup. While hardly any C64 game will make use of the second fire button, there are actually quite a number of Amiga games who do. Out of this reason I decided to add a second fire button for v4 of this project, while V2 and V3 just have one logical fire button. The stripboard of V3 can be easily upgraded to v4. To make it work for both left-handed and right-handed players v4 has 4 physical fire buttons, one set for each side. Interestingly left-handed vs. right-handed use seems to be a much more widespread difference than the percentage of left-handed people would lead you to think. Even right-handed people who grew up with consoles like NES will put the joystick into their left hand, because that's what they're used to with the D-pad. So keeping it handedness-neutral was an important design consideration.
* **2nd fire button calibratable** - Unfortunately the C64 and the Amiga use different ways to interpret the second fire button. For the C64 it is pressed when it gets 5V on pin 9 of the DB9 port, and for the Amiga it is pressed when it has 0V. To cater for both I'm letting the ATtiny84A send the signal for this fire button to the old machines. I can program the ATtiny to use the initial state of the button as input to determine which mode it should put it in. So if you want it to work on a C64, then press the 2nd fire button while you plug it in, or while you start your C64, it will then know what to do. If you want to use it on the Amiga it works by default, no need to press while plugging it in.
* **Period Electronics** - I really wanted to use electronics that could have been around in the 80's, to make the Joystick as authentic as possible, so I decided for the classic [7400-series](https://en.wikipedia.org/wiki/7400-series_integrated_circuits) line, the 74LS family from 1983 to be precise.
* **Rapid Fire** - The NE555 chip I use to create the rapid fire is even older, it's from 1973. There are [C64 magazines from the 80's](http://pitsch.de/stuff/magicdisk64/md8712.htm) that describe how to use it to create rapid fire. However, after checking the values they used against [this very helpful (german) site](https://www.elektronik-kompendium.de/sites/slt/1208271.htm) I concluded that the fire frequency it would have produced would be pretty useless, so I decided to create my own layout with the recommendations for astable mulitivibrators from the aforementioned web site, using the NE555. I'm aware that it would be possible in V3+ to use an analog input of the microcontroller to create a rapid fire, and another free output for the LED, but for now I want to keep it retro. It seems wrong to me that the microcontroller that powers the rapid fire is almost more powerful than the actual computer it is connected to. But maybe one day I will create a USB only model, and will then put everything into the microcontroller. Maybe with more fine grained control over button push and release cycles, to even auto-fire the big beam weapons, e.g. in Katakis. **Note**: Currently rapid fire is only available for the primary fire button.
* **Adjustable Rapid Fire** - Most of the period joysticks I came across had totally unusable rapid fire, it would only produce volleys with long breaks in between. Now I understand that different games and computers will work best at different firing frequencies, due to how often they would manage to query the I/O chip the Joystick is attached to. So the only way to have rapid fire that works across a wide range of games and machines is one that is adjustable. Joyweiler's frequency ranges from 2.8 Hz to 29.5 Hz. The C64 usually has its sweet spot somewhere around 1/2, while the Amiga can just about handle close to full speed in some games.
* **Stripboard** - I don't know much about how to create PCBs, so I decided to go for what I know, which is [Strip boards](https://en.wikipedia.org/wiki/Stripboard). I enjoyed the challenge of putting the layout together, reminds me of playing Sudoku. Also I enjoy soldering, I think it's very therapeutic, like knitting, so I didn't mind the extra work compared to a custom-made PCB. By the way, I timed how long it took me to get to the same state as a pre-made PCB would give me (that's the strip cuts, and all the plain wires in), and it was around 2 hours, without trying to be quick. **PCB Update:** An austrian company named Emilum generously contributed a PCB layout to this project, see below for more information.
* **Stripboard layout done with Tgif** - I've had a look at all the Free Software tools available to bring the circuit board layout into a proper electronic format. I checked out `VeroRoute`, `DIYLC`, `BlackBoard` and `PcbNew`. The first three of them I found utterly unusable and frustrating to use. I would have probably been able to use `PcbNew` for stripboards, but it wasn't really designed with that use case in mind, so wouldn't have been as clear and simple I would have liked it. So I found the quickest and clearest way was to use [Tgif](https://en.wikipedia.org/wiki/Tgif_(program)).
* **ATtiny84A without external chrystal** - I use [V-USB](https://www.obdev.at/products/vusb/index.html) to make the microcontroller be able to talk USB. V-USB offers you to use different frequencies to run the chip at, most of which need an external chrystal to clock the chip. Such a chrystal would need to be connected to two fixed pins of the chip. At the same time USB D+ needs to be connected to whatever pin gives you INT0 (that's the highest priority interrupt), and D- needs to be connected to the same port (a port is a set of pins that together build a byte out of their up to 8 bits/pins). From first principle I should need no more than 10 pins (that's 2 for power, 4 for the 4 joystick directions, and 2 for the fire buttons, plus 2 for the USB data lines; nearest size actual AVR chip comes in 14 pins). However the ATtinyX4 range's pin layout is such that port B, which is only a 4 bit (i.e 4 pins) port, contains both the 2 clock connections, INT0, and the weak RESET pin. Using the RESET pin as a regular I/O pin instead has 2 disadvantages: Firstly once it is configured to be an I/O pin the chip is not programmable with regular means, but only with so-called `HVPP`, which requires special expensive hardware. So if you don't have that hardware you would essentially brick your chip if you flash a malfunctioning firmware onto it. Secondly the RESET pin sources/sinks a lot less current, so I would have needed additional electronics to boost its signal. So, since V-USB requires that both D+ and D- are attached to the same port, and that D+ is attached to INT0, I had a dilemma. Connecting D+ to INT0 was no problem. But to connect D- as well to the same port I either had to connect it to one of the 2 ports that also take the clock signal (and therefore give up the option to use an external clock signal), or I attach it to RESET. I found ability to easily re-program the chip more important, so decided for giving up the option to have an external clock. That, however, has another downside: The driver V-USB uses to make the internal oscillator of the microcontroller be usable as a clock source is a lot larger in compiled bytes than the ones that use an external chrystal time clock. Currently my firmware image is almost 2300 bytes large, and therefore too large for the ATtiny24A (with 2K flash). So I needed to go for the ATtiny44A (with 4K flash) or for the ATtiny84A (with 8K flash). V5 of this project requires the ATtiny84A.
* **Measuring Hertz** - Essential to this was to be able to measure milliseconds like `millis()` on Ardunio. [This article](https://adnbr.co.uk/articles/counting-milliseconds) helped me immensely to implement this on plain AVR C.
* **7-segment display** - I've used [this library](https://github.com/lpodkalicki/attiny-tm1637-library) to get the 4 digit 7-segment display going. I had to adjust and rewrite bits to make it work with a display which features 4 decimal points instead of a single colon for clock-style operations.
* **Code growth** - [This article](http://www.github.com/abcminiuser/avr-tutorials/blob/master/ManagingLargeProjects/Output/ManagingLargeProjects.pdf?raw=true) was so helpful to learn how to split up the C code of this project into manageable chunks. Also the article about [Progmem](http://www.github.com/abcminiuser/avr-tutorials/blob/master/Progmem/Output/Progmem.pdf?raw=true) was an excellent help. 

## How to operate second Fire Button on C64, Amiga, VICE, FS-UAE
* **V4:**
    * To make the 2nd fire button work on a C64 you have to keep it pressed down while you plug it in. This calibrates it for C64 use. This is necessary because Amiga and C64 use different standards to work out whether it's pressed or not.
    * On the Amiga you don't need to keep it pressed down while plugging it in, Amiga-mode is the default.
* **V5:**
    * C64 vs. Amiga mode can be set in the builtin configuration menu. This setting will be stored beyond unplugging the joystick.
* On VICE at the time of writing the 2nd fire button isn't supported yet. There is an [open feature request](https://sourceforge.net/p/vice-emu/feature-requests/189/) pending.
* On FS-UAE you need to put [this config file](/config/edge_records_joyweiler.ini) into `FS-UAE/Controllers/`. If you rename your USB vendor and/or device name then you need to change the name of the ini file accordingly. See [here](https://fs-uae.net/custom-controller-configuration) for more information.

## How to test the Joystick
* On the C64 I use the fantastic new tool named [Game Controller Tester](https://csdb.dk/release/?id=179703).
* On Linux I use [jstest-gtk](https://jstest-gtk.gitlab.io/).
* On the Amiga I'm still on the lookout for a good tool, that works with Kickstart 1.3, and shows the 2nd fire button. Please contact me if you have a suggestion!

## How to make

### The skillset you'll need (or will acquire in the process):
* Soldering and cable making
* Drilling and filing the aluminium case
* Use of a multimeter
* Compiling and flashing firmware for microcontrollers (optional)

### The tools you'll need
* Soldering equipment. I used:
    * Soldering iron (I used the Mercury 703.212)
    * Solder (I used 1mm diameter)
    * Desoldering pump (or something else to de-solder)
    * Small wire cutter
    * Needlenose pliers
    * Rubber band (to hold parts in place while soldering them)
    * Strip board cutting tool to cut strip tracks (I used a 3mm HSS drill instead, worked as good, if not better)
    * Ideally a Multimeter, at the very least a continuity tester
* Internal cables:
    * I bought myself a Dupont crimp tool set and plyers. They are fiddly, but couldnt' find a better solution for single row pin headers
    * Crimping the flat ribbon cables for dual row pin headers is a piece of cake, Just used universal pliers for that.
* Metal case:
    * Electric drill, ideally pillar drill
    * HSS drills, 3.2mm (3mm will just about do as well), 4.5mm, 6mm, 7mm, 9mm
    * Countersink drill for M3
    * Files, I had one super small set, and one small one
    * Scriber, ideally pair of compasses-style scriber (You won't see pencil markings during filing)
    * Centre punch & hammer
    * Measurement tools
    * Little metal saw to trim the potentiometer shaft
* Compiling and flashing microcontroller (optional):
    * A computer capable of running the AVR tool (`gcc-avr`, `binutils-avr`, `avr-libc`, `avrdude` on Debian)
    * Hardware to flash the microcontroller. I used a self-made `dapa` cable for the parallel port of my laptop. Other cheap options are to use a Raspberry Pi and its GPIO ports, or a USB adapter like Vusbtiny. The programming port J7 pinout is compatible to the 6 pin version as described in [section 3.1.3 of this PDF](https://community.atmel.com/sites/default/files/project_files/ATAVRISP_User_Guide.pdf). There are plenty of cheap programmers out there that can be used.

### My Parts List
See [parts.ods](parts.ods).

I've also created lists on reichelt.de to make ordering easier:

| URL | List |
| --- | ---- |
| https://www.reichelt.de/my/1635659 | Joystick V4/V5 parts |
| https://www.reichelt.de/my/1635665 | Items where you need to choose colours etc. |
| https://www.reichelt.de/my/1635664 | Bits I've classified as consumable material |

Please double-check these though before ordering for mistakes and completeness.

### Cables

![Joyweiler's guts](img/bare_guts_v3.jpg)

The Zero Delay USB board is usually shipped with loads of cables to
* connect the board to USB
* connect the board to the Joystick
* connect the board to the Fire buttons

For both Joyweiler V2 and V3+ you need these cables:
* A cable to connect the Joystick to your board. Sanwa Joysticks are shipped with a connection cable which at the one side has a 5 pin plug, and at the other has open wire ends. You can either solder these wires to the Joystick directly, and plug in the other end to your board, or you plug it in as intended to the Joystick, and crimp a 5 pin dupont plug to the other end or solder it directly to the board.
* A 2x3 flat ribbon cable you need to make yourself to connect the rapid fire switch and pot and fire signal LED to your board. Pinout can be found in the electric diagram.
* A 2x5 flat ribbon cable you need to make yourself for the D-Sub 9-pin connector. Pinout can be found in the electric diagram, but is a straight forward 1:1 pin mapping between the 2x5 head and the D-Sub pins.

For Joyweiler V2 you don't connect the Joystick directly to the Zero Delay board, but to the board you create yourself. There is 3 cables running from your board to the Zero Delay board:
* Two 2 pin cables, one for power supply, one for the Fire button (you can make them yourself out of 2 of the many unneeded cables that come with the Zero Delay board to connect it to buttons)
* One 5 pin cable for the Joystick directions. This cable is usually shipped with the Zero Delay board.

For Joyweiler V3+ you only need a connection from the inside of your USB keystone adapter to a Dupont 4 pin. I suggest you open the inside part of your keystone adapter, remove the mounted jack by de-soldering and bend it open with plyers, and solder the 4 wires directly onto the board the internal USB jack was originally soldered to. Make sure you keep the internal half of the keystone plastic case intact, since you need to re-attach it so that the external USB-B plug doesn't slip when you plug in a cable (will make sense when you see it).

For Joyweiler V5 you need to make a cable to connect the 2x3 pin programming SPI connection to the 1x4 pin on the 7-segment TM1637 unit. The SPI connector (J7 in the diagram) has double function, for both programming the ATtiny initially, and for connecting the 7-segment unit in normal operation.

### Future Proofing with USB-C

![Deluxe Back](img/deluxe_back.jpg)

You might have a modern computer which already has USB-C. In that case you might want to future-proof the joystick by attaching a USB-C keystone inlet instead of the suggested USB-B one. There's two options of back side case drawings to cater for either horizontal or vertical mounts of the keystone holder, for USB-C you'll probably end up using the horizontal option. When you buy the plug for the internal cable (from inside keystone to stripboard 4 pin), make sure that the plug uses a pull-down resistor of 5.1K from the USB-C A5 pin to GND. If you buy a USB-C plug with the more widespread 56K Pull-Up resistor between A5 and Vbus it won't work. For more details see the [USB-C spec zip file](https://usb.org/sites/default/files/USB%20Type-C_20190130.zip) (in there the PDF called `USB Type-C Specification Release 1.3.pdf`, section 3.5.4 (good, table 3-15 also gives you the pinout for your internal cable from USB-C to 4 pin stripboard). Section 3.5.2 shows you the 56K variant you want to avoid. The magic word to google for to find the right adapters is "OTG", or "Type C to USB OTG Connector Adapter", or something along these lines.

It's funny to see the DB9 Gameport from [1977](https://en.wikipedia.org/wiki/Atari_joystick_port) and the cutting-edge USB-C port next to each other, and essentially performing exactly the same job.

## Version History

### Version features
Version 2 relies for the USB communications on a 3rd party PCB usually named something like Quimat Zero Delay Arcade USB Encoder. 

**Features V2:**
* You don't need to program the microcontroller yourself.
* It gets shipped with a lot of useful cables.

**Features V3+:**
* You need to install only one circuit board in your case. The Zero Delay board annoyingly needs M2 screws to be mounted, which are more difficult to source than M3 screws, esp. in countersunk form.
* The extra parts for a V3+ board are cheaper than a Zero Delay board.
* You have full control over the USB device name that appears when you connect it.
* The USB driver will not display any surplus axis or buttons that are not implemented.

**Features V4:**
* There's a PCB for this version
* This version introduces a second logical fire button. It works for both Amiga and C64. To use on C64 hold the 2nd fire button down while plugging it in to calibrate it. Since the microcontroller is used to drive the 2nd fire button for old computers it is not possible to upgrade V2 to have a 2nd fire button.
* V3 stripboards are easily upgradeable to v4. Only 2 solder spots have to be adjusted, and one cut strip track needs to be bridged. All other changes are pure additions with no need to change already existing parts and wire bridges.

**Features V5:**
* This version adds a 4 digit 7-segment display, which displays the FIRE1 frequency in Hz, that's both the rapid fire frequency, and the manual (human) firing frequency.
* This version will display a rolling greeting banner when the joystick is plugged in (Can be switched off).
* This version lets you get into a configuration menu if you press FIRE2 while plugging in the Joystick. This configuration menu is displayed on the 7-segment display, and is operated via the joystick. Settings are permanently saved once FIRE2 is pressed again. Up/Down moves around in the menu, Left/Right changes the settings. These settings are configurable:
    * Line 1: C64 / Amiga500 mode
    * Line 2: Display on(1) or off(0)
    * Line 3: Display brightness from 0 to 7
    * Line 4: Greeting banner on(1) or off(0)
* Currently there is unfortunately no PCB for V5, only Stripboard version.
* Only few resoldering jobs are required to upgrade V4 to V5.
* After the microcontroller has been programmed, it has to be plugged in once via USB to a computer, since it uses the USB signal to calibrate itself accurately to display the correct frequency value. This calibration value is then permanently stored for future use of the joystick. If this isn't done the microcontroller will not know how long a second is, and the Hertz display will be incorrect.

But a Video says more than 1000 words. Click the following image to watch it on Youtube:
[![Joyweiler V5](https://i.ytimg.com/vi_webp/-EysjGoyjyY/maxresdefault.webp)](https://youtu.be/-EysjGoyjyY "Joyweiler V5")

### Which Documents for Which Version
| File | V2 | V3 | V4 | V5 |
| --- | --- |--- | --- | --- |
| Stripboard layout as PDF | [stripboard_v2.pdf](stripboard_v2.pdf) | [stripboard_v3.pdf](stripboard_v3.pdf) | [stripboard_v4.pdf](stripboard_v4.pdf) | [stripboard_v5.pdf](stripboard_v5.pdf) |
| Stripboard layout in Tgif format | [stripboard_v2.obj](stripboard_v2.obj) | [stripboard_v3.obj](stripboard_v3.obj) | [stripboard_v4.obj](stripboard_v4.obj) | [stripboard_v5.obj](stripboard_v5.obj) |
| Electric Diagram as PDF | [joyweiler_v2.pdf](joyweiler_v2.pdf) | [joyweiler_v3.pdf](joyweiler_v3.pdf) | [joyweiler_v4.pdf](joyweiler_v4.pdf) | [joyweiler_v5.pdf](joyweiler_v5.pdf) |
| Electric Diagram in Eeschema format | [joyweiler2.sch](joyweiler2.sch) | [joyweiler3.sch](joyweiler3.sch) | [joyweiler4.sch](joyweiler4.sch) | [joyweiler5.sch](joyweiler5.sch) |
| PCB in Eagle format | n/a | n/a | [pcb/Retro-Joystick_V_1.brd](pcb/Retro-Joystick_V_1.brd) | n/a |
| PCB as PDF | n/a | n/a | [pcb/Retro-Joystick_V_1_Bestueckungsdruck.pdf](pcb/Retro-Joystick_V_1_Bestueckungsdruck.pdf) | n/a |
| Electric Diagram for PCB in Eagle format | n/a | n/a | [pcb/Retro-Joystick_V_1.sch](pcb/Retro-Joystick_V_1.sch) | n/a |
| Electric Diagram for PCB as PDF | n/a | n/a | [pcb/Retro-Joystick_Schaltplan.pdf](pcb/Retro-Joystick_Schaltplan.pdf) | n/a |
| Microcontroller build dir | [V4/usb](../../tree/V4/usb) | [V4/usb](../../tree/V4/usb) | [V4/usb](../../tree/V4/usb) | [usb](usb) |

## Notes on Potentiometer
During the work for V5 I noticed how sub-optimal the linear 50K pot is, since all the interesting frequencies for gaming happened in the last 3/4 of its rotation range, so it was quite hard to home in on exact frequencies. I needed a logarithmic pot, in reverse audio, or antilog layout. Since they seem to be very rare to find, especially in 4mm shaft diameter format, I ended up ordering a few with [OMEG](https://www.omeg.co.uk). Their service was very fast and helpful. Here is the pot I used: [Datasheet OW16BU](https://www.omeg.co.uk/wp-content/uploads/2020/02/OW16BU.pdf). I've used the one under "Resistance law C - Antilog(Reverse Audio)", full part number is `OW16BU 47KC`. The linear pot works as well, so I won't take it out of the regular Reichelt parts list, but if you want something even better (but more expensive) treat yourself to one of these.

## Notes on PCB

![PCB front](img/pcb_front.jpg)
![PCB back](img/pcb_back.jpg)

When the friendly people from [austrian LED lighting company Emilum](http://www.emilum.com) saw the [article in the german Make magazine](https://heise.de/-4408414) they made contact and offered to develop a professional PCB for this project. Also Emilum offer to sell the PCB at cost price. Contact them via retrojoystick@emilum.com for further information. Features of the PCB:

* It implements Version 4 of this project.
* Its connectors are fully pin compatible to the stripboard.
* The PCB is fully through-hole, so soldering it couldn't be easier.
* It is small enough to work with the gratis version of Eagle.
* It is generously published under a Free Licence: [CC-BY-SA](https://creativecommons.org/licenses/by-sa/4.0/)

## Notes on Stripboard

![Stripboard V4 back](img/stripboard_back.jpg)
![Stripboard V4 front](img/stripboard_front.jpg)

* All horizontal lines as represented by the squared paper have electric connectivity implicitly. Due to that all parts and wire bridges are put in vertically, which keeps the layout tidy.
* The double-rowed pin heads need very filigree cutting between their two rows of holes (as outlined by the zig-zag line in the layout), e.g. with a Stanley knife. Best you make this the first step before soldering on any parts, since it helps to be able to lay down the board flat onto your worktop to prevent it from breaking when you apply pressure.
* Another thing you want to do at the start is to drill the 4 holes in each corner.
* The cells marked with `X` mean that the track needs to be cut at this place. Easiest is to use a 3mm drill with your hand. Test every one of these breaks with a continuity tester, even the smallest amount of copper left can cause the board to malfunction, and you can't see them without magnification. It is a good idea to think ahead with these cuts. It is easier to make a track cut before you solder any surrounding fields.
* If you use V2 you'll need to solder on a 1x2 pin head to the Zero Delay USB board, to supply your board with +5V and GND.
* To test rudimentary functionality of the board I used a USB charger to supply the 5V. The board needs `J1` to be connected to work. If it works you should see a blinking LED if you switch on rapid fire.
* The IC socket with builtin capacitor (see parts list) is for the 7407.

## Building The Case

![Metalwork](img/metalwork.jpg)

The following case design files are in [/case](/case) as `.obj` (which is editable with [Tgif](https://en.wikipedia.org/wiki/Tgif_(program))), as `.png` and as `.pdf`:

* **case_top_regular** - This is the drawing compatible with the knob in the parts list. When making my prototype I thought to make switch and knob look symmetrical I need to place their center identical distances to the case center. But that didn't look symmetrical in the end. So I've decided to place the knob's and switche's inner edge an identical distance from the center of the case. You can see that with the dashed lines.
* **case_top_deluxe** - I've created one with an Aluminium knob with bigger dimensions.
* **case_back_vert_usb** - This is for the keystone holder and USB-B inlet from the parts list. You've got to be very precise when making this, since the space between the bottom of the potentiometer and the top of the circuit board is very limited, and just about fits the USB inlet vertically. Also the space to plug in complete USB-A plugs inside is very limited. Better you break up the inside of the keystone inlet and solder the cable directly on it, as described above. In any case, take measure first, with the Sanwa Joystick in place.
* **case_back_horiz_usb** - This is for the keystone holder mounted horizontally, to cater e.g. for a USB-C keystone inlet (See above for further info).

I didn't make any drawings for the floor of the case, since it is trivial to drill the 8 (V2) or 4 (V3+) holes into it. Just note that the V2 extra Zero Delay board needs M2 screws to attach, therefore smaller drillings and countersunk holes.

I'd suggest that you don't mount the circuit boards until the top part of the case is completely built, so that you can see where you'll have enough space for the circuit boards with all its plugs in. I've mounted the Zero delay board on the extreme left, and the stripboard immediately next to it.

| Note: |
| ----- |
| Documentation for a 3D-printable case has been kindly released. See "Links" Section below. |

## Compiling And Flashing The ATtiny Firmware
This is only needed for V3+.

* Prereq's on a Debian-based system are the following `deb` packages (installable with `apt install <pkgname>`:
    * gcc-avr
    * binutils-avr
    * avr-libc
    * avrdude

* Then clone or download this repo and go to the `/usb` directory (see [table here](#which-documents-for-which-version) for the right location of that directory).
* Run `make hex`. This should produce a file called `main.hex`
* Adjust `Makefile` in the line towards the top that starts with `AVRDUDE =` to contain your programmer instead of `dapa`. You can see the list of supported devices [here](https://www.nongnu.org/avrdude/user-manual/avrdude_4.html#Option-Descriptions) under `-c`.
* Adjust `usbconfig.h` in the line starting with `#define USB_CFG_VENDOR_NAME` with your own name, according to the rules outlined by [V-USB](/usb/usbdrv/USB-IDs-for-free.txt). Also change `#define USB_CFG_VENDOR_NAME_LEN` to the number of letters in your vendor name.
* with your programmer attached to J7 (the SPI programming connector) on the V3+ board, run `make program`, which should do 2 things, first flash the firmware to the chip, and set the fuses of the chip as described in the `FUSE_L` and `FUSE_H` lines of the `Makefile`. If this comes back successful you're done.

# Links
| Description | Link |
| --- | --- | 
| A 3D Print case for this project (thanks to amrotzek) | https://www.thingiverse.com/thing:4045578 |
| Article in german Make Magazine | https://heise.de/-4408414 |
| German Newsflash about PCB release |  https://heise.de/-4592272 |
| Another 3D Print case, in Arcade Stick layout, with display for V5 (thanks to Schlumpf999) | [Thingiverse](https://www.thingiverse.com/thing:4793846) [Prusaprinters](https://www.prusaprinters.org/prints/59951-joystick-gamepad-retro-arcade-for-pc-usb-c-and-c64) |

# Contact
You can reach me at stephan@edge-records.net

![Deluxe Front](img/deluxe_front.jpg)
![Prototype](img/prototype_front.jpg)
