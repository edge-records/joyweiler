// INCLUDES
#include <util/atomic.h>
#include "tm1637.h"
#include "millis.h"

#include "firefreq.h"
#include "globaldefinitions.h"

// DEFINES

// LOCAL FUNCTION DECLARATIONS
/**
 * Print Hertz in ii.dH format
 * where i = integer
 *       d = one decimal place
 *       H = the letter H for Hertz
 */
static void HertzPrint(uint16_t myint);

// VARIABLE INITIALIZATIONS
uint32_t fire1s [32]; // Record of all times fire1 was pressed, round robin
uint8_t f1arraycounter = 0, formerjoy = 0;

// GLOBAL FUNCTION DEFINITIONS
void countFire1(uint8_t currentjoy) {
  uint8_t ncj = ~currentjoy;
  uint32_t currmill = millis();

  //Find all Fire1 raising flanks
  if ((formerjoy & _BV(FIRE1)) && (ncj & _BV(FIRE1))) {
    // Debounce firebutton for 30ms or less (this allows for up to 33.3Hz)
    // This wasn't enough to properly debounce SANWA OBSF, but is better than nothing.
    // Can't raise it any further since fire frequencies up to 30Hz are realistic.
    if ( currmill - (fire1s[(f1arraycounter -1) % 32]) > 30 ) {
      fire1s[f1arraycounter % 32] = currmill; //Count them into round robin array
      f1arraycounter++;
    }
  }
  formerjoy = currentjoy;
}

void HertzCalc(uint32_t currentms) {
  // we seem to have to use 32 bit values all the way throughout the calculation
  // since otherwise weird stuff happens
  uint32_t onesecago = currentms - 1000;
  uint32_t oldestf1 = currentms; // pre-charge with too high value
  uint32_t youngestf1 = onesecago; // pre-charge with too low value
  uint32_t delta;
  uint32_t fires = 0;
  uint8_t i;
  uint32_t hertz = 0; // fires and hertz never reach 32 bit, but create funny errors when of different length than delta

  // Find oldest and youngest value within the last 1 second in rotating fire1s array
  for ( i = 0; i < 32; i++ ) {
    if ( fire1s[i] >= onesecago ) {
      if ( fire1s[i] < oldestf1 ) {
        oldestf1 = fire1s[i];
      } else if ( fire1s[i] > youngestf1 ) {
        youngestf1 = fire1s[i];
      }
      // Find how many fire presses we had within the last second
      fires++;
    }
  }

  if (youngestf1 > oldestf1) delta = youngestf1 - oldestf1; // time between first and last rising edge of Fire1
  if (fires >= 1) {
    fires--; // Cut off the last unfinished fire
    // get number of fires in relation to 1 second
    // This isn't actually true Hertz, but for now Hertz * 100, after rounding Hertz * 10.
    // Because we want to display one decimal point without using float. True Hertz would be fires * 1000.
    hertz = ((fires * 100000) / delta);
    // Simple rounding routine
    if ((hertz % 5) <= 4) {
      hertz /= 10;
    } else {
      hertz = (hertz + 10) / 10;
    }
  }

  HertzPrint(hertz);
}

// LOCAL FUNCTION DEFINITIONS
static void HertzPrint(uint16_t myint) {
  volatile uint8_t n,
          digit;

  for (n = 3; n > 0; n--) {
    digit = myint % 10;
    if (digit == 0 && myint == 0) {
      if (n == 1) {
        digit = 11; // don't print leading 0
      } else {
        digit = 0;
      }
    }
    myint /= 10;
    TM1637_display_digit(n -1, digit);
  }
  TM1637_display_decdot(1); // Set decimal dot after 2 digits
  TM1637_display_segments(3, 0b01110110); // the letter "H"
}

