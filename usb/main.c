/*
 * Joyweiler
 *	
 * based on project hid-mouse, a very simple HID example by Christian Starkjohann, OBJECTIVE DEVELOPMENT Software GmbH
 * and ATtiny2313 USB Joyadapter firmware by Grigori Goronzy.	
 * adapted for ATtiny84A
 * 
 * Name: main.c
 * Project: Joyweiler
 * Author: Stephan Eckweiler (Andreas Paul, Christian Starkjohann, Grigori Goronzy) 
 * Creation Date: 2008-04-07
 * Tabsize: 4
 * 
 * License: GNU GPL v2 (see License.txt), GNU GPL v3 or proprietary (CommercialLicense.txt)
 * This Revision: $Id: $
 */

#include <avr/io.h>
#include <avr/wdt.h>
#include <avr/interrupt.h>  /* for sei() */
#include <util/delay.h>     /* for _delay_ms() */

#include <avr/pgmspace.h>   /* required by usbdrv.h */
#include "usbdrv.h"
#include "osccal.h"
#include "usb.h"

#include "firefreq.h"
#include <avr/eeprom.h>
#include "tm1637.h"
#include "millis.h"

#include "confmenu.h"
#include "globaldefinitions.h"

uint8_t activecolumn;

PROGMEM const uint8_t JOYWEILER_segments[17] = {
  0x00, // empty
  0x00, // empty
  0x00, // empty
  0x1F, // J
  0x5C, // o
  0x6E, // y
  0xBC, // 1/2 W
  0x0E, // 2/2 W
  0x79, // E
  0x06, // I
  0x38, // L
  0x79, // E
  0x50, // r
  0x00, // empty
  0x00, // empty
  0x00, // empty
  0x00, // empty
};

/* ------------------------------------------------------------------------- */

//int __attribute__((noreturn)) main(void) {
int main(void) {
  uint8_t calforsec = eeprom_read_byte(CALSTORE);
  volatile uint8_t bootBannOffs = 0; // Boot Banner Offset
  uint32_t milliseconds_current;
  uint32_t milliseconds_since = 0;

  activecolumn = ( eeprom_read_byte(ACTIVECOLMNSTORE) == 0xFF ) ? 0x26 : eeprom_read_byte(ACTIVECOLMNSTORE);

  // Setup 4x7 Segment display
  TM1637_init(1/*enable*/, ((activecolumn & 0x1C) >> 2)/*brightness*/);

  // Setup Milliseconds counter
  // CTC mode, Clock/8
  TCCR1B |= (1 << WGM12) | (1 << CS10);

  // Load the high byte, then the low byte
  // into the output compare
  OCR1AH = (CTC_MATCH_OVERFLOW >> 8);
  OCR1AL = CTC_MATCH_OVERFLOW;

  // Enable the compare match interrupt
  TIMSK1 |= (1 << OCIE1A);

  TM1637_clear();

  uchar   i;
  uchar currentjoy, lastjoy = 0xfa; 
  DDRB |= (1 << FIRE2_OUT); // Configure FIRE2_OUT as an output pin

  /* If Fire2 is pressed while plugging in Joystick, open Configuration menu
   */
  if (JOY & _BV(FIRE2)) {
    runmenu();
  }

  wdt_enable(WDTO_1S);
  /* Even if you don't use the watchdog, turn it off here. On newer devices,
   * the status of the watchdog (on/off, period) is PRESERVED OVER RESET!
   */
  /* RESET status: all port bits are inputs without pull-up.
   * That's the way we need D+ and D-. Therefore we don't need any
   * additional hardware initialization.
   */

  usbInit();
  usbDeviceDisconnect();  /* enforce re-enumeration, do this while interrupts are disabled! */
  i = 0;
  while(--i){             /* fake USB disconnect for > 250 ms */
    wdt_reset();
    _delay_ms(1);
  }
  usbDeviceConnect();

  if (calforsec != 0xff) {
    OSCCAL = calforsec;
  }
  sei();

  for(;;){                /* main event loop */
    wdt_reset();
    usbPoll();

    currentjoy = JOY;

    if(usbInterruptIsReady()){
      /* called after every poll of the interrupt endpoint */

      if(lastjoy != currentjoy)
      {
        lastjoy = currentjoy;
        buildReport(currentjoy);
        usbSetInterrupt((void *)&reportBuffer, sizeof(reportBuffer));
      }

    }

    /* Send button 2 state to FIRE2_OUT, either C64 or Amiga style */
    // if C64
    if ((activecolumn & 0x01) == 0) {
      if (JOY & _BV(FIRE2)) {
        PORTB |= (1 << FIRE2_OUT);
      } else {
        PORTB &= ~(1 << FIRE2_OUT);
      }
    // if Amiga
    } else {
      if (JOY & _BV(FIRE2)) {
        PORTB &= ~(1 << FIRE2_OUT);
      } else {
        PORTB |= (1 << FIRE2_OUT);
      }
    }

    // if display is on
    if ((activecolumn & 0x02) >> 1 == 1) {
      countFire1(currentjoy);

      milliseconds_current = millis();
      // if banner isn't done yet and enabled
      if ( bootBannOffs < 14 && ((activecolumn & 0x20) >> 5) == 1) {
        if (milliseconds_current - milliseconds_since > 250) {
          TM1637_display_segments(0, pgm_read_byte(&JOYWEILER_segments[bootBannOffs]));
          TM1637_display_segments(1, pgm_read_byte(&JOYWEILER_segments[bootBannOffs + 1]));
          TM1637_display_segments(2, pgm_read_byte(&JOYWEILER_segments[bootBannOffs + 2]));
          TM1637_display_segments(3, pgm_read_byte(&JOYWEILER_segments[bootBannOffs + 3]));
          bootBannOffs++;
          if (bootBannOffs == 3) { bootBannOffs++; }
          milliseconds_since = milliseconds_current;
        }
      } else if (milliseconds_current - milliseconds_since > 500) {
        HertzCalc(milliseconds_current);
        if (calforsec != OSCCAL) {
          eeprom_write_byte(CALSTORE, OSCCAL);
          calforsec = eeprom_read_byte(CALSTORE);
        }
        milliseconds_since = milliseconds_current;
      }
    } else {
      // Disable display
      TM1637_init(0,0);
    }
  }
}
/* ------------------------------------------------------------------------- */
