#include <stdint.h>
#include <avr/pgmspace.h>   /* required by usbdrv.h */
#include "usbdrv.h"

#include "usb.h"
#include "globaldefinitions.h"

/* ------------------------------------------------------------------------- */
/* ----------------------------- USB interface ----------------------------- */
/* ------------------------------------------------------------------------- */

PROGMEM const char usbHidReportDescriptor[48] = {
  0x05, 0x01,                    // USAGE_PAGE (Generic Desktop)
  0x09, 0x04,                    // USAGE (Joystick)
  0xa1, 0x01,                    // COLLECTION (Application)
  0x09, 0x01,                    //   USAGE (Pointer)
  0xA1, 0x00,                    //   COLLECTION (Physical)
  0x09, 0x30,                    //     USAGE (X)
  0x09, 0x31,                    //     USAGE (Y)
  0x15, 0x81,                    //     LOGICAL_MINIMUM (-127)
  0x25, 0x7F,                    //     LOGICAL_MAXIMUM (127)
  0x75, 0x08,                    //     REPORT_SIZE (8)
  0x95, 0x02,                    //     REPORT_COUNT (2)
  0x81, 0x02,                    //     INPUT (Data,Var,Abs)
  0x05, 0x09,                    //     USAGE_PAGE (Button)
  0x19, 0x01,                    //     USAGE_MINIMUM
  0x29, 0x02,                    //     USAGE_MAXIMUM
  0x15, 0x00,                    //     LOGICAL_MINIMUM (0)
  0x25, 0x01,                    //     LOGICAL_MAXIMUM (1)
  0x95, 0x02,                    //     REPORT_COUNT (2)
  0x75, 0x01,                    //     REPORT_SIZE (1)
  0x81, 0x02,                    //     INPUT (Data,Var,Abs)
  0x95, 0x01,                    //     REPORT_COUNT (1)
  0x75, 0x06,                    //     REPORT_SIZE (6)
  0x81, 0x03,                    //     INPUT (Const,Var,Abs)
  0xC0,                          //   END_COLLECTION
  0xC0,                          // END COLLECTION
};

/* The data described by this descriptor consists of 3 bytes:
 *     X7 X6 X5 X4 X3 X2 X1 X0 .... 8 bit signed relative coordinate x
 *     Y7 Y6 Y5 Y4 Y3 Y2 Y1 Y0 .... 8 bit signed relative coordinate y
 *      .  .  .  .  .  . B1 B0 .... two bits for Fire button
 */

/* ------------------------------------------------------------------------- */

report_t reportBuffer;
static uint8_t    idleRate;   /* repeat rate for keyboards, never used for mice */

usbMsgLen_t usbFunctionSetup(uint8_t data[8]) {
  usbRequest_t    *rq = (void *)data;

  /* The following requests are never used. But since they are required by
   * the specification, we implement them in this example.
   */
  if((rq->bmRequestType & USBRQ_TYPE_MASK) == USBRQ_TYPE_CLASS){    /* class request type */
    if(rq->bRequest == USBRQ_HID_GET_REPORT){  /* wValue: ReportType (highbyte), ReportID (lowbyte) */
      /* we only have one report type, so don't look at wValue */
      usbMsgPtr = (void *)&reportBuffer;
      return sizeof(reportBuffer);
    }else if(rq->bRequest == USBRQ_HID_GET_IDLE){
      usbMsgPtr = &idleRate;
      return 1;
    }else if(rq->bRequest == USBRQ_HID_SET_IDLE){
      idleRate = rq->wValue.bytes[1];
    }
  }else{
    /* no vendor specific requests implemented */
  }
  return 0;   /* default for not implemented requests: return no data back to host */
}

/* ------------------------------------------------------------------------- */

/*  0x15, 0x81,                    //     LOGICAL_MINIMUM (-127)
    0x25, 0x7F,                    //     LOGICAL_MAXIMUM (127)
    */
void buildReport(uint8_t joy) {
  uint8_t x = 0, y = 0, fire = 0;
  uint8_t njoy = ~joy;

  if (njoy & _BV(FIRE1)) fire = 0x01; //Fire1//
  if (joy & _BV(FIRE2)) fire |= 0x02; //Fire2// 
  if (njoy & _BV(UP)) y = 0x81;    //Up//
  if (njoy & _BV(DOWN)) y = 0x7f;    //Down//
  if (njoy & _BV(RIGHT)) x = 0x7f;    //Right//
  if (njoy & _BV(LEFT)) x = 0x81;    //Left//

  reportBuffer.buttonMask = fire;
  reportBuffer.dx = x;
  reportBuffer.dy = y;
}

