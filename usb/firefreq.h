#ifndef FIREFREQ_H
#define FIREFREQ_H

/**
 * Count all raising flanks of Fire1
 * Also try to debounce the button as good as possible
 */
void countFire1(uint8_t currentjoy);

/**
 * Calculate current frequency, based on
 *   - times Fire1 was counted with countFire1,
 *   - between the current millisecond and one second ago,
 *   - corrected against a full second.
 * Will round against 2 decimal places, to have 1 decimal place accuracy
 * Will call HertzPrint, to update display
 */
void HertzCalc(uint32_t currentms);

#endif /* FIREFREQ_H */
