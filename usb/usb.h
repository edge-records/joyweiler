#ifndef USB_H
#define USB_H

typedef struct{
  uint8_t   dx;
  uint8_t   dy;
  uint8_t   buttonMask;
}report_t;

extern report_t reportBuffer;

usbMsgLen_t usbFunctionSetup(uint8_t data[8]);

void buildReport(uint8_t joy);

#endif /* USB_H */
