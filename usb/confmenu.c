/**
 * activecolumn byte: 7 6 5 4 3 2 1 0
 *                    | | | | | | | L 0 = C64, 1 = A500                , default 0
 *                    | | | | | | L__ 0 = Display Off, 1 = Display On  , default 1
 *                    | | | L_L_L____ 3 bytes, 0-7, brightness         , default 001
 *                    | | L__________ 0 = Greeting off, 1 = Greeting on, default 1
 *                    | |
 *                    Default: 0x26 (38)
 *
 * define menu entry rows, default row 0, default entry *:
 *   0: C64 *    | A500     | Fire2 Mode
 *   1: dSP0     | dSP1 *   | Display on or off (MAIN loop only)
 *   2: bri0     | bri7     | Brightness, 0-7, default 1
 *   3: Grt0     | Grt1 *   | Greeting on or off
 */

#include <avr/eeprom.h>
#include <avr/pgmspace.h>
#include <avr/wdt.h>
#include <util/delay.h>
#include "confmenu.h"
#include "globaldefinitions.h"
#include "tm1637.h"

PROGMEM const uint8_t ROW00[] = {
  0x39, // C
  0x7D, // 6
  0x66, // 4
  0x00, // empty
};

PROGMEM const uint8_t ROW01[] = {
  0x77, // A
  0x6D, // 5
  0x3F, // 0
  0x3F, // 0
};

PROGMEM const uint8_t ROW1stem[] = {
  0x5E, // d
  0x6D, // S
  0x73, // P
};

PROGMEM const uint8_t ROW2stem[] = {
  0x7C, // b
  0x50, // r
  0x10, // i
};

PROGMEM const uint8_t ROW3stem[] = {
  0x3D, // G
  0x50, // r
  0x78, // t
};

// 4 separate segments from Progmem
static void displaytype1(const uint8_t *elements) {
  uint8_t n;
  for (n=0; n < 4; n++) {
    TM1637_display_segments(n, pgm_read_byte(elements++));
  }
}

// 3 segments from Progmem, plus one digit
static void displaytype2(const uint8_t *elements, uint8_t column) {
  uint8_t n;
  for (n=0; n < 3; n++) {
    TM1637_display_segments(n, pgm_read_byte(elements++));
  }
  TM1637_display_segments(3, pgm_read_byte(&_digit2segments[column]));
}

static void displayrowcolumn(uint8_t row, uint8_t column) {
  switch(row) {
    case 0 :
      if ( column == 0 ) {
        displaytype1(ROW00);
      }
      if ( column == 1 ) {
        displaytype1(ROW01);
      }
      break;
    case 1 :
      displaytype2(ROW1stem, column);
      break;
    case 2 :
      displaytype2(ROW2stem, column);
      break;
    case 3 :
      displaytype2(ROW3stem, column);
      break;
  }
}

static int getcolumn(uint8_t row) {
  uint8_t column;

  switch(row) {
    case 0 :
      column = activecolumn & 0x01;
      break;
    case 1 :
      column = (activecolumn & 0x02) >> 1;
      break;
    case 2 :
      column = (activecolumn & 0x1C) >> 2;
      break;
    case 3 :
      column = (activecolumn & 0x20) >> 5;
      break;
  }

  return(column);
}

// direction 1=right, 0=left
// Shifts the 3 bits for brightness between 0 (0b000) and 7 (0b111)
static int shiftbrightness(int8_t direction) {
  int8_t column;

  column = (activecolumn & 0x1C) >> 2;
  if (direction == 1) {
    if (column < 7) column++;
  } else {
    if (column > 0) column--;
  }
  activecolumn &= 0xE3;
  activecolumn |= (column << 2);

  return(column);
}


int runmenu(void) {
  uint8_t currentjoy,
          negcjoy,
          lastjoy = 0xfa,
          currentrow = 0;



  for(;;){
    wdt_reset();
    currentjoy = JOY;
    negcjoy = ~currentjoy;

    displayrowcolumn(currentrow, getcolumn(currentrow));

    if ((lastjoy & _BV(UP)) && (negcjoy & _BV(UP))) {
      if (currentrow > 0) currentrow--;
    }
    if ((lastjoy & _BV(DOWN)) && (negcjoy & _BV(DOWN))) {
      if (currentrow < 3) currentrow++;
    }
    if ((lastjoy & _BV(RIGHT)) && (negcjoy & _BV(RIGHT))) {
      switch(currentrow) {
        case 0 :
          activecolumn |= 0x01;
          break;
        case 1 :
          activecolumn |= 0x02;
          break;
        case 2 :
          TM1637_init(1/*enable*/, shiftbrightness(1)/*brightness*/);
          break;
        case 3 :
          activecolumn |= 0x20;
          break;
      }
    }
    if ((lastjoy & _BV(LEFT)) && (negcjoy & _BV(LEFT))) {
      switch(currentrow) {
        case 0 :
          activecolumn &= 0xFE;
          break;
        case 1 :
          activecolumn &= 0xFD;
          break;
        case 2 :
          TM1637_init(1/*enable*/, shiftbrightness(0)/*brightness*/);
          break;
        case 3 :
          activecolumn &= 0xDF;
          break;
      }
    }

    // Exit Configuration Menu
    if ((~lastjoy & _BV(FIRE2)) && (currentjoy & _BV(FIRE2))) {
      if (eeprom_read_byte(ACTIVECOLMNSTORE) != activecolumn) eeprom_write_byte(ACTIVECOLMNSTORE, activecolumn);
      return(activecolumn);
    }

    lastjoy = currentjoy;
    // To debounce keys and joystick
    _delay_ms(10);
  }

}



