#ifndef GLOBALDEFINITIONS_H

// EEPROM bytes for config
#define CALSTORE 0
#define ACTIVECOLMNSTORE 1

#define CTC_MATCH_OVERFLOW (F_CPU / 1000)

#define JOY (PINA)

#define UP    PA0
#define DOWN  PA1
#define LEFT  PA3
#define RIGHT PA2
#define FIRE1 PA7
#define FIRE2 PA6 

#define FIRE2_OUT PB0

extern uint8_t activecolumn;

#endif
