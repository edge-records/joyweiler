EESchema Schematic File Version 4
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Joyweiler Electric Diagram"
Date "2018-12-29"
Rev "2.1"
Comp "https://gitlab.com/edge-records/joyweiler"
Comment1 "http://creativecommons.org/licenses/by-sa/4.0/"
Comment2 "Licenced under Creative Commons Attribution-ShareAlike 4.0 International License."
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L 74xx:74LS00 U2
U 1 1 5C0B11BA
P 5200 1800
F 0 "U2" H 5200 2125 50  0000 C CNN
F 1 "74LS00" H 5200 2034 50  0000 C CNN
F 2 "" H 5200 1800 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls00" H 5200 1800 50  0001 C CNN
	1    5200 1800
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS00 U2
U 2 1 5C0B140B
P 6050 1800
F 0 "U2" H 6050 2125 50  0000 C CNN
F 1 "74LS00" H 6050 2034 50  0000 C CNN
F 2 "" H 6050 1800 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls00" H 6050 1800 50  0001 C CNN
	2    6050 1800
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0101
U 1 1 5C0B184D
P 1100 1150
F 0 "#PWR0101" H 1100 1000 50  0001 C CNN
F 1 "+5V" H 1115 1323 50  0000 C CNN
F 2 "" H 1100 1150 50  0001 C CNN
F 3 "" H 1100 1150 50  0001 C CNN
	1    1100 1150
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_SPST SW2
U 1 1 5C0B2C63
P 4000 2000
F 0 "SW2" H 3950 2200 50  0000 C CNN
F 1 "Fire" H 3950 2100 50  0000 C CNN
F 2 "" H 4000 2000 50  0001 C CNN
F 3 "" H 4000 2000 50  0001 C CNN
	1    4000 2000
	1    0    0    -1  
$EndComp
$Comp
L joyweiler:NE555 U1
U 1 1 5C0B3AC6
P 2600 2450
F 0 "U1" H 2900 2950 50  0000 L CNN
F 1 "NE555" H 2850 2850 50  0000 L CNN
F 2 "" H 2600 2450 50  0001 C CNN
F 3 "" H 2600 2450 50  0001 C CNN
	1    2600 2450
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 5C0B3F29
P 1600 1850
F 0 "R2" H 1400 1900 50  0000 L CNN
F 1 "4.7K" H 1350 1800 50  0000 L CNN
F 2 "" V 1530 1850 50  0001 C CNN
F 3 "~" H 1600 1850 50  0001 C CNN
	1    1600 1850
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 5C0B3F9A
P 1600 1450
F 0 "R1" H 1400 1500 50  0000 L CNN
F 1 "1K" H 1400 1400 50  0000 L CNN
F 2 "" V 1530 1450 50  0001 C CNN
F 3 "~" H 1600 1450 50  0001 C CNN
	1    1600 1450
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 5C0B4AAB
P 2700 4000
F 0 "C2" H 2815 4046 50  0000 L CNN
F 1 "100nF" H 2815 3955 50  0000 L CNN
F 2 "" H 2738 3850 50  0001 C CNN
F 3 "~" H 2700 4000 50  0001 C CNN
	1    2700 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	2700 2900 2700 3850
Wire Wire Line
	2100 2650 2000 2650
Wire Wire Line
	2100 2450 2000 2450
Wire Wire Line
	2000 2450 2000 2650
Wire Wire Line
	1100 1150 1300 1150
Wire Wire Line
	2500 2050 2500 1150
Connection ~ 2500 1150
Wire Wire Line
	2500 1150 2700 1150
Wire Wire Line
	2700 2000 2700 1150
Connection ~ 2700 1150
$Comp
L power:GND #PWR0102
U 1 1 5C0B88B7
P 1100 4450
F 0 "#PWR0102" H 1100 4200 50  0001 C CNN
F 1 "GND" H 1105 4277 50  0000 C CNN
F 2 "" H 1100 4450 50  0001 C CNN
F 3 "" H 1100 4450 50  0001 C CNN
	1    1100 4450
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS04 U3
U 6 1 5C0C1BFF
P 7550 4750
F 0 "U3" V 7550 5000 50  0000 C CNN
F 1 "74LS04" V 7650 5000 50  0000 C CNN
F 2 "" H 7550 4750 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS04" H 7550 4750 50  0001 C CNN
	6    7550 4750
	0    1    1    0   
$EndComp
$Comp
L 74xx:74LS04 U3
U 1 1 5C0C1CB0
P 7050 4750
F 0 "U3" V 7050 5000 50  0000 C CNN
F 1 "74LS04" V 7150 5000 50  0000 C CNN
F 2 "" H 7050 4750 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS04" H 7050 4750 50  0001 C CNN
	1    7050 4750
	0    1    1    0   
$EndComp
$Comp
L 74xx:74LS04 U3
U 2 1 5C0C1D02
P 6550 4750
F 0 "U3" V 6550 5000 50  0000 C CNN
F 1 "74LS04" V 6650 5000 50  0000 C CNN
F 2 "" H 6550 4750 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS04" H 6550 4750 50  0001 C CNN
	2    6550 4750
	0    1    1    0   
$EndComp
$Comp
L 74xx:74LS04 U3
U 3 1 5C0C1D5F
P 6050 4750
F 0 "U3" V 6050 5000 50  0000 C CNN
F 1 "74LS04" V 6150 5000 50  0000 C CNN
F 2 "" H 6050 4750 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS04" H 6050 4750 50  0001 C CNN
	3    6050 4750
	0    1    1    0   
$EndComp
$Comp
L Device:CP C1
U 1 1 5C16358F
P 1800 4000
F 0 "C1" H 1918 4046 50  0000 L CNN
F 1 "10uF" H 1918 3955 50  0000 L CNN
F 2 "" H 1838 3850 50  0001 C CNN
F 3 "~" H 1800 4000 50  0001 C CNN
	1    1800 4000
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_SPST SW3
U 1 1 5C217379
P 4000 2400
F 0 "SW3" H 3950 2600 50  0000 C CNN
F 1 "Fire" H 3950 2500 50  0000 C CNN
F 2 "" H 4000 2400 50  0001 C CNN
F 3 "" H 4000 2400 50  0001 C CNN
	1    4000 2400
	1    0    0    -1  
$EndComp
$Comp
L 74xx_IEEE:7407 U4
U 5 1 5C3BCB17
P 8750 1550
F 0 "U4" H 9100 1900 50  0000 C CNN
F 1 "74LS07" H 9150 1800 50  0000 C CNN
F 2 "" H 8750 1550 50  0001 C CNN
F 3 "" H 8750 1550 50  0001 C CNN
	5    8750 1550
	1    0    0    -1  
$EndComp
$Comp
L 74xx_IEEE:7407 U4
U 3 1 5C3BCC96
P 8750 2250
F 0 "U4" H 9100 2600 50  0000 C CNN
F 1 "74LS07" H 9150 2500 50  0000 C CNN
F 2 "" H 8750 2250 50  0001 C CNN
F 3 "" H 8750 2250 50  0001 C CNN
	3    8750 2250
	1    0    0    -1  
$EndComp
$Comp
L 74xx_IEEE:7407 U4
U 2 1 5C71DB3A
P 8750 2900
F 0 "U4" H 9100 3250 50  0000 C CNN
F 1 "74LS07" H 9150 3150 50  0000 C CNN
F 2 "" H 8750 2900 50  0001 C CNN
F 3 "" H 8750 2900 50  0001 C CNN
	2    8750 2900
	1    0    0    -1  
$EndComp
$Comp
L 74xx_IEEE:7407 U4
U 1 1 5C7943B9
P 8750 3550
F 0 "U4" H 9100 3900 50  0000 C CNN
F 1 "74LS07" H 9150 3800 50  0000 C CNN
F 2 "" H 8750 3550 50  0001 C CNN
F 3 "" H 8750 3550 50  0001 C CNN
	1    8750 3550
	1    0    0    -1  
$EndComp
$Comp
L 74xx_IEEE:7407 U4
U 6 1 5C7EAEAB
P 8750 4200
F 0 "U4" H 9100 4550 50  0000 C CNN
F 1 "74LS07" H 9150 4450 50  0000 C CNN
F 2 "" H 8750 4200 50  0001 C CNN
F 3 "" H 8750 4200 50  0001 C CNN
	6    8750 4200
	1    0    0    -1  
$EndComp
$Comp
L Device:R_POT R3
U 1 1 5C2C47B3
P 1600 2750
F 0 "R3" H 1500 2850 50  0000 R CNN
F 1 "50K" H 1500 2750 50  0000 R CNN
F 2 "" H 1600 2750 50  0001 C CNN
F 3 "~" H 1600 2750 50  0001 C CNN
	1    1600 2750
	1    0    0    -1  
$EndComp
$Comp
L joyweiler:Conn_exploded-Connector_generic J1_1
U 1 1 5C349B20
P 1800 3000
F 0 "J1_1" V 1850 2800 50  0000 L CNN
F 1 "Pin" V 1845 3048 50  0001 L CNN
F 2 "" H 1800 3000 50  0001 C CNN
F 3 "~" H 1800 3000 50  0001 C CNN
	1    1800 3000
	0    1    1    0   
$EndComp
$Comp
L joyweiler:Conn_exploded-Connector_generic J1_2
U 1 1 5C3DF889
P 1600 2300
F 0 "J1_2" V 1650 2100 50  0000 L CNN
F 1 "Pin" V 1645 2348 50  0001 L CNN
F 2 "" H 1600 2300 50  0001 C CNN
F 3 "~" H 1600 2300 50  0001 C CNN
	1    1600 2300
	0    1    1    0   
$EndComp
Wire Wire Line
	2700 4150 2700 4300
Wire Wire Line
	2700 4300 2500 4300
Connection ~ 2500 4300
Wire Wire Line
	2500 2850 2500 4300
Wire Wire Line
	1100 4450 1100 4300
Wire Wire Line
	1100 4300 1400 4300
$Comp
L joyweiler:Conn_exploded-Connector_generic J1_4
U 1 1 5C5D5378
P 3250 2150
F 0 "J1_4" V 3250 2198 50  0000 L CNN
F 1 "Pin" V 3295 2198 50  0001 L CNN
F 2 "" H 3250 2150 50  0001 C CNN
F 3 "~" H 3250 2150 50  0001 C CNN
	1    3250 2150
	0    1    1    0   
$EndComp
$Comp
L Switch:SW_SPST SW1
U 1 1 5C5DB503
P 3650 1700
F 0 "SW1" H 3650 1935 50  0000 C CNN
F 1 "Rapid Fire" H 3650 1844 50  0000 C CNN
F 2 "" H 3650 1700 50  0001 C CNN
F 3 "" H 3650 1700 50  0001 C CNN
	1    3650 1700
	1    0    0    -1  
$EndComp
$Comp
L joyweiler:Conn_exploded-Connector_generic J1_6
U 1 1 5C5DE5A0
P 4150 1700
F 0 "J1_6" H 4150 1567 50  0000 C CNN
F 1 "Pin" H 4150 1566 50  0001 C CNN
F 2 "" H 4150 1700 50  0001 C CNN
F 3 "~" H 4150 1700 50  0001 C CNN
	1    4150 1700
	-1   0    0    1   
$EndComp
Wire Wire Line
	3850 1700 3950 1700
$Comp
L Device:R R4
U 1 1 5C5EAE2D
P 4400 1450
F 0 "R4" H 4470 1496 50  0000 L CNN
F 1 "100K" H 4470 1405 50  0000 L CNN
F 2 "" V 4330 1450 50  0001 C CNN
F 3 "~" H 4400 1450 50  0001 C CNN
	1    4400 1450
	1    0    0    -1  
$EndComp
$Comp
L Device:R R5
U 1 1 5C5EAF31
P 4750 1450
F 0 "R5" H 4820 1496 50  0000 L CNN
F 1 "100K" H 4820 1405 50  0000 L CNN
F 2 "" V 4680 1450 50  0001 C CNN
F 3 "~" H 4750 1450 50  0001 C CNN
	1    4750 1450
	1    0    0    -1  
$EndComp
Wire Wire Line
	5500 1800 5550 1800
Wire Wire Line
	5650 1800 5650 1700
Wire Wire Line
	5650 1700 5750 1700
Wire Wire Line
	5750 1900 5650 1900
Wire Wire Line
	5650 1900 5650 1800
Connection ~ 5650 1800
$Comp
L joyweiler:Conn_exploded-Connector_generic J3_1
U 1 1 5C61DDAA
P 3500 2750
F 0 "J3_1" V 3500 2798 50  0000 L CNN
F 1 "Pin" V 3545 2798 50  0001 L CNN
F 2 "" H 3500 2750 50  0001 C CNN
F 3 "~" H 3500 2750 50  0001 C CNN
	1    3500 2750
	0    1    1    0   
$EndComp
$Comp
L joyweiler:Conn_exploded-Connector_generic J4_1
U 1 1 5C61DED7
P 3750 2750
F 0 "J4_1" V 3750 2798 50  0000 L CNN
F 1 "Pin" V 3795 2798 50  0001 L CNN
F 2 "" H 3750 2750 50  0001 C CNN
F 3 "~" H 3750 2750 50  0001 C CNN
	1    3750 2750
	0    1    1    0   
$EndComp
$Comp
L joyweiler:Conn_exploded-Connector_generic J3_2
U 1 1 5C621C37
P 4450 2000
F 0 "J3_2" H 4450 1900 50  0000 C CNN
F 1 "Pin" H 4450 1950 50  0001 C CNN
F 2 "" H 4450 2000 50  0001 C CNN
F 3 "~" H 4450 2000 50  0001 C CNN
	1    4450 2000
	-1   0    0    1   
$EndComp
$Comp
L joyweiler:Conn_exploded-Connector_generic J4_2
U 1 1 5C621CD0
P 4450 2400
F 0 "J4_2" H 4450 2300 50  0000 C CNN
F 1 "Pin" H 4450 2350 50  0001 C CNN
F 2 "" H 4450 2400 50  0001 C CNN
F 3 "~" H 4450 2400 50  0001 C CNN
	1    4450 2400
	-1   0    0    1   
$EndComp
Wire Wire Line
	3500 2550 3500 2000
Wire Wire Line
	3500 2000 3800 2000
Wire Wire Line
	3750 2550 3750 2400
Wire Wire Line
	3750 2400 3800 2400
Wire Wire Line
	4200 2000 4250 2000
Wire Wire Line
	4200 2400 4250 2400
Wire Wire Line
	3100 2450 3250 2450
Wire Wire Line
	3250 2450 3250 2350
Wire Wire Line
	3250 1950 3250 1700
Wire Wire Line
	3250 1700 3450 1700
Wire Wire Line
	3500 2950 3500 4300
Wire Wire Line
	3500 4300 2700 4300
Connection ~ 2700 4300
Wire Wire Line
	3750 2950 3750 4300
Wire Wire Line
	3750 4300 3500 4300
Connection ~ 3500 4300
$Comp
L joyweiler:Joystick-joystick SW4
U 1 1 5C68BB63
P 4200 3050
F 0 "SW4" H 4240 3298 50  0000 C CNN
F 1 "Joystick" H 4240 3207 50  0000 C CNN
F 2 "" H 4200 3050 50  0001 C CNN
F 3 "" H 4200 3050 50  0001 C CNN
	1    4200 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	3750 4300 3900 4300
Connection ~ 3750 4300
Wire Wire Line
	5150 3050 5150 2250
Wire Wire Line
	5350 3250 5350 2900
Wire Wire Line
	5350 3450 5350 3550
Wire Wire Line
	5150 3650 5150 4200
Wire Wire Line
	5150 4200 7450 4200
$Comp
L Device:R R6
U 1 1 5C6ADE93
P 6550 1450
F 0 "R6" H 6620 1496 50  0000 L CNN
F 1 "47K" H 6620 1405 50  0000 L CNN
F 2 "" V 6480 1450 50  0001 C CNN
F 3 "~" H 6550 1450 50  0001 C CNN
	1    6550 1450
	1    0    0    -1  
$EndComp
$Comp
L Device:R R7
U 1 1 5C6ADFA7
P 6850 1450
F 0 "R7" H 6920 1496 50  0000 L CNN
F 1 "47K" H 6920 1405 50  0000 L CNN
F 2 "" V 6780 1450 50  0001 C CNN
F 3 "~" H 6850 1450 50  0001 C CNN
	1    6850 1450
	1    0    0    -1  
$EndComp
$Comp
L Device:R R8
U 1 1 5C6AE054
P 7150 1450
F 0 "R8" H 7220 1496 50  0000 L CNN
F 1 "47K" H 7220 1405 50  0000 L CNN
F 2 "" V 7080 1450 50  0001 C CNN
F 3 "~" H 7150 1450 50  0001 C CNN
	1    7150 1450
	1    0    0    -1  
$EndComp
$Comp
L Device:R R9
U 1 1 5C6AE10E
P 7450 1450
F 0 "R9" H 7520 1496 50  0000 L CNN
F 1 "47K" H 7520 1405 50  0000 L CNN
F 2 "" V 7380 1450 50  0001 C CNN
F 3 "~" H 7450 1450 50  0001 C CNN
	1    7450 1450
	1    0    0    -1  
$EndComp
Wire Wire Line
	6550 1300 6550 1150
Connection ~ 6550 1150
Wire Wire Line
	6550 1150 6850 1150
Wire Wire Line
	6850 1300 6850 1150
Connection ~ 6850 1150
Wire Wire Line
	6850 1150 7150 1150
Wire Wire Line
	7150 1300 7150 1150
Connection ~ 7150 1150
Wire Wire Line
	7150 1150 7450 1150
Wire Wire Line
	7450 1300 7450 1150
Wire Wire Line
	6550 1600 6550 2250
Connection ~ 6550 2250
Wire Wire Line
	6550 2250 8200 2250
Wire Wire Line
	6850 1600 6850 2900
Connection ~ 6850 2900
Wire Wire Line
	6850 2900 8200 2900
Wire Wire Line
	7150 1600 7150 3550
Connection ~ 7150 3550
Wire Wire Line
	7150 3550 8200 3550
Wire Wire Line
	7450 1600 7450 4200
Connection ~ 7450 4200
Wire Wire Line
	6350 1800 8050 1800
Wire Wire Line
	8050 1800 8050 1550
Wire Wire Line
	8050 1550 8200 1550
$Comp
L joyweiler:Conn_exploded-Connector_generic J7_1
U 1 1 5C791F3B
P 4000 4000
F 0 "J7_1" V 4000 4048 50  0000 L CNN
F 1 "Pin" V 4045 4048 50  0001 L CNN
F 2 "" H 4000 4000 50  0001 C CNN
F 3 "~" H 4000 4000 50  0001 C CNN
	1    4000 4000
	0    1    1    0   
$EndComp
$Comp
L joyweiler:Conn_exploded-Connector_generic J7_2
U 1 1 5C792062
P 4800 3050
F 0 "J7_2" H 4800 3000 50  0000 C CNN
F 1 "Pin" H 4800 3000 50  0001 C CNN
F 2 "" H 4800 3050 50  0001 C CNN
F 3 "~" H 4800 3050 50  0001 C CNN
	1    4800 3050
	-1   0    0    1   
$EndComp
$Comp
L joyweiler:Conn_exploded-Connector_generic J7_3
U 1 1 5C79226F
P 4800 3250
F 0 "J7_3" H 4800 3200 50  0000 C CNN
F 1 "Pin" H 4800 3200 50  0001 C CNN
F 2 "" H 4800 3250 50  0001 C CNN
F 3 "~" H 4800 3250 50  0001 C CNN
	1    4800 3250
	-1   0    0    1   
$EndComp
$Comp
L joyweiler:Conn_exploded-Connector_generic J7_4
U 1 1 5C792326
P 4800 3450
F 0 "J7_4" H 4800 3400 50  0000 C CNN
F 1 "Pin" H 4800 3400 50  0001 C CNN
F 2 "" H 4800 3450 50  0001 C CNN
F 3 "~" H 4800 3450 50  0001 C CNN
	1    4800 3450
	-1   0    0    1   
$EndComp
$Comp
L joyweiler:Conn_exploded-Connector_generic J7_5
U 1 1 5C7923DE
P 4800 3650
F 0 "J7_5" H 4800 3600 50  0000 C CNN
F 1 "Pin" H 4800 3600 50  0001 C CNN
F 2 "" H 4800 3650 50  0001 C CNN
F 3 "~" H 4800 3650 50  0001 C CNN
	1    4800 3650
	-1   0    0    1   
$EndComp
Wire Wire Line
	4000 4200 4000 4300
Wire Wire Line
	4000 3650 4000 3800
Wire Wire Line
	4400 3650 4600 3650
Wire Wire Line
	4400 3450 4600 3450
Wire Wire Line
	4400 3250 4600 3250
Wire Wire Line
	4400 3050 4600 3050
Wire Wire Line
	5000 3050 5150 3050
Wire Wire Line
	5000 3250 5350 3250
Wire Wire Line
	5000 3450 5350 3450
Wire Wire Line
	5000 3650 5150 3650
Wire Wire Line
	5150 2250 6050 2250
Wire Wire Line
	7450 4200 7550 4200
Wire Wire Line
	5350 3550 7050 3550
Wire Wire Line
	5350 2900 6550 2900
Wire Wire Line
	6050 4450 6050 2250
Connection ~ 6050 2250
Wire Wire Line
	6050 2250 6550 2250
Wire Wire Line
	6550 4450 6550 2900
Connection ~ 6550 2900
Wire Wire Line
	6550 2900 6850 2900
Wire Wire Line
	7050 4450 7050 3550
Connection ~ 7050 3550
Wire Wire Line
	7050 3550 7150 3550
Wire Wire Line
	7550 4450 7550 4200
Connection ~ 7550 4200
Wire Wire Line
	7550 4200 8200 4200
Connection ~ 5550 1800
Wire Wire Line
	5550 1800 5650 1800
$Comp
L Device:LED D3
U 1 1 5C82263E
P 4550 4600
F 0 "D3" H 4550 4700 50  0000 C CNN
F 1 "LED" H 4550 4800 50  0000 C CNN
F 2 "" H 4550 4600 50  0001 C CNN
F 3 "~" H 4550 4600 50  0001 C CNN
	1    4550 4600
	1    0    0    1   
$EndComp
$Comp
L joyweiler:Conn_exploded-Connector_generic J1_3
U 1 1 5C822772
P 4950 4600
F 0 "J1_3" H 4950 4700 50  0000 C CNN
F 1 "Pin" H 4950 4466 50  0001 C CNN
F 2 "" H 4950 4600 50  0001 C CNN
F 3 "~" H 4950 4600 50  0001 C CNN
	1    4950 4600
	-1   0    0    1   
$EndComp
$Comp
L joyweiler:Conn_exploded-Connector_generic J1_5
U 1 1 5C822877
P 4150 4600
F 0 "J1_5" H 4150 4700 50  0000 C CNN
F 1 "Pin" H 4150 4466 50  0001 C CNN
F 2 "" H 4150 4600 50  0001 C CNN
F 3 "~" H 4150 4600 50  0001 C CNN
	1    4150 4600
	-1   0    0    1   
$EndComp
Wire Wire Line
	4350 4600 4400 4600
Wire Wire Line
	4700 4600 4750 4600
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J5
U 1 1 5C86713C
P 10050 2900
F 0 "J5" H 10100 2600 50  0000 C CNN
F 1 "C64" H 10100 2500 50  0000 C CNN
F 2 "" H 10050 2900 50  0001 C CNN
F 3 "~" H 10050 2900 50  0001 C CNN
	1    10050 2900
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x03_Odd_Even J1
U 1 1 5C86D924
P 2500 6350
F 0 "J1" H 2550 6667 50  0000 C CNN
F 1 "Rapid Fire" H 2550 6600 50  0000 C CNN
F 2 "" H 2500 6350 50  0001 C CNN
F 3 "~" H 2500 6350 50  0001 C CNN
	1    2500 6350
	1    0    0    -1  
$EndComp
Wire Wire Line
	9700 2250 9700 2800
Wire Wire Line
	9700 2800 9850 2800
Wire Wire Line
	9300 2250 9700 2250
Wire Wire Line
	9300 3550 10650 3550
Wire Wire Line
	10650 3550 10650 2700
Wire Wire Line
	10650 2700 10350 2700
Wire Wire Line
	9300 2900 9400 2900
Wire Wire Line
	9400 2900 9400 3450
Wire Wire Line
	9400 3450 10550 3450
Wire Wire Line
	10550 3450 10550 2800
Wire Wire Line
	10550 2800 10350 2800
Wire Wire Line
	9300 4200 9600 4200
Wire Wire Line
	9600 4200 9600 2700
Wire Wire Line
	9600 2700 9850 2700
Wire Wire Line
	10750 1550 10750 2900
Wire Wire Line
	10750 2900 10350 2900
Wire Wire Line
	9850 3000 9500 3000
Wire Wire Line
	9300 1550 10750 1550
Wire Wire Line
	9500 3000 9500 1150
$Comp
L Diode:1N5817 D2
U 1 1 5C8D8617
P 8050 1150
F 0 "D2" H 8050 1366 50  0000 C CNN
F 1 "1N5817" H 8050 1275 50  0000 C CNN
F 2 "Diode_THT:D_DO-41_SOD81_P10.16mm_Horizontal" H 8050 975 50  0001 C CNN
F 3 "http://www.vishay.com/docs/88525/1n5817.pdf" H 8050 1150 50  0001 C CNN
	1    8050 1150
	1    0    0    -1  
$EndComp
Wire Wire Line
	7900 1150 7450 1150
Connection ~ 7450 1150
Wire Wire Line
	8200 1150 9500 1150
NoConn ~ 9850 2900
NoConn ~ 9850 3100
NoConn ~ 10350 3100
$Comp
L Connector_Generic:Conn_01x05 J6
U 1 1 5C91EDA0
P 6800 5550
F 0 "J6" V 6673 5262 50  0000 R CNN
F 1 "USB Joystick" V 6764 5262 50  0000 R CNN
F 2 "" H 6800 5550 50  0001 C CNN
F 3 "~" H 6800 5550 50  0001 C CNN
	1    6800 5550
	0    -1   1    0   
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J8
U 1 1 5C924D49
P 1300 5550
F 0 "J8" V 1173 5362 50  0000 R CNN
F 1 "USB Power" V 1264 5362 50  0000 R CNN
F 2 "" H 1300 5550 50  0001 C CNN
F 3 "~" H 1300 5550 50  0001 C CNN
	1    1300 5550
	0    -1   1    0   
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J2
U 1 1 5C930AD5
P 5450 5550
F 0 "J2" V 5323 5362 50  0000 R CNN
F 1 "USB Fire" V 5414 5362 50  0000 R CNN
F 2 "" H 5450 5550 50  0001 C CNN
F 3 "~" H 5450 5550 50  0001 C CNN
	1    5450 5550
	0    -1   1    0   
$EndComp
Wire Wire Line
	7550 5050 7550 5250
Wire Wire Line
	7550 5250 7000 5250
Wire Wire Line
	7000 5250 7000 5350
Wire Wire Line
	7050 5050 7050 5150
Wire Wire Line
	7050 5150 6900 5150
Wire Wire Line
	6900 5150 6900 5350
Wire Wire Line
	6550 5050 6550 5150
Wire Wire Line
	6550 5150 6800 5150
Wire Wire Line
	6800 5150 6800 5350
Wire Wire Line
	6050 5050 6050 5250
Wire Wire Line
	6050 5250 6700 5250
Wire Wire Line
	6700 5250 6700 5350
NoConn ~ 5450 5350
NoConn ~ 6600 5350
$Comp
L Diode:1N5817 D1
U 1 1 5C96B032
P 1300 4000
F 0 "D1" V 1254 4079 50  0000 L CNN
F 1 "1N5817" V 1345 4079 50  0000 L CNN
F 2 "Diode_THT:D_DO-41_SOD81_P10.16mm_Horizontal" H 1300 3825 50  0001 C CNN
F 3 "http://www.vishay.com/docs/88525/1n5817.pdf" H 1300 4000 50  0001 C CNN
	1    1300 4000
	0    1    1    0   
$EndComp
Wire Wire Line
	1300 4150 1300 5350
Wire Wire Line
	1300 3850 1300 1150
Connection ~ 1300 1150
Wire Wire Line
	1400 5350 1400 4300
Connection ~ 1400 4300
$Comp
L power:GND #PWR?
U 1 1 5C9AC70E
P 10150 4450
F 0 "#PWR?" H 10150 4200 50  0001 C CNN
F 1 "GND" H 10155 4277 50  0000 C CNN
F 2 "" H 10150 4450 50  0001 C CNN
F 3 "" H 10150 4450 50  0001 C CNN
	1    10150 4450
	1    0    0    -1  
$EndComp
Wire Wire Line
	10350 3000 10750 3000
Wire Wire Line
	10750 3000 10750 3650
Wire Wire Line
	10750 3650 10150 3650
Wire Wire Line
	10150 3650 10150 4450
Wire Wire Line
	2000 6250 2300 6250
Wire Wire Line
	2000 6350 2300 6350
Wire Wire Line
	2000 6450 2300 6450
Wire Wire Line
	3150 6250 2800 6250
Wire Wire Line
	3150 6350 2800 6350
Wire Wire Line
	3150 6450 2800 6450
Text Label 2850 6250 0    50   ~ 0
Potentiometer_Fire_Speed_IN
Text Label 1250 6250 0    50   ~ 0
Potentiometer_Fire_Speed_OUT
Text Label 2050 6350 0    50   ~ 0
LED_IN
Text Label 2000 6450 0    50   ~ 0
LED_OUT
Text Label 2850 6350 0    50   ~ 0
Rapid_Fire_Toggle_Switch_IN
Text Label 2850 6450 0    50   ~ 0
Rapid_Fire_Toggle_Switch_OUT
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J5
U 1 1 5CA2E54E
P 2500 6950
F 0 "J5" H 2550 6650 50  0000 C CNN
F 1 "C64" H 2550 6550 50  0000 C CNN
F 2 "" H 2500 6950 50  0001 C CNN
F 3 "~" H 2500 6950 50  0001 C CNN
	1    2500 6950
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 6750 2300 6750
Wire Wire Line
	2000 6850 2300 6850
Wire Wire Line
	2000 6950 2300 6950
Wire Wire Line
	2000 7050 2300 7050
Wire Wire Line
	2000 7150 2300 7150
Wire Wire Line
	3150 6750 2800 6750
Wire Wire Line
	3150 6850 2800 6850
Wire Wire Line
	3150 6950 2800 6950
Wire Wire Line
	3150 7050 2800 7050
Wire Wire Line
	3150 7150 2800 7150
Text Label 2100 6750 0    50   ~ 0
Up
Text Label 2100 6850 0    50   ~ 0
Left
Text Label 2100 7050 0    50   ~ 0
+5V
Text Label 2850 6750 0    50   ~ 0
Down
Text Label 2850 6850 0    50   ~ 0
Right
Text Label 2850 6950 0    50   ~ 0
Fire
Text Label 2850 7050 0    50   ~ 0
GND
NoConn ~ 2000 6950
NoConn ~ 2000 7150
NoConn ~ 3150 7150
Wire Notes Line
	4050 5950 1200 5950
Text Notes 3050 6050 0    50   ~ 0
Ribbon Cable Connectors
Wire Notes Line
	4050 5950 4050 7450
Wire Notes Line
	4050 7450 1200 7450
Wire Notes Line
	1200 7450 1200 5950
Text Notes 4150 6600 0    50   ~ 0
J1 Rapid Fire\nJ2 USB Fire\nJ3 Fire1\nJ4 Fire2\nJ5 C64\nJ6 USB Joystick\nJ7 Sanwa Joystick\nJ8 USB Power
Text Notes 5350 6450 0    50   ~ 0
U1 NE555 (Timer)\nU2 74LS00 (NAND)\nU3 74LS04 (Inverter)\nU4 74LS07 (Open Collector)
Text Notes 5350 7150 0    50   ~ 0
SW1 Rapid Fire Toggle\nSW2 Fire1\nSW3 Fire2\nSW4 Sanwa Joystick
Text Notes 5350 6750 0    50   ~ 0
D1 USB Power\nD2 C64 Power\nD3 Fire LED
Text Notes 4150 7450 0    50   ~ 0
R1 Timer\nR2 Timer\nR3 Potentiometer Fire Speed\nR4 Pull-up Rapid Fire\nR5 Pull-up Manual Fire\nR6 Pull-up Left\nR7 Pull-up Right\nR8 Pull-up Down\nR9 Pull-up Up\nR10 LED Series Resistor
Wire Wire Line
	4650 2000 4750 2000
Wire Wire Line
	4650 2400 4750 2400
Wire Wire Line
	4750 1900 4900 1900
Wire Wire Line
	4750 2000 4750 1900
Connection ~ 4750 2000
Wire Wire Line
	4750 2400 4750 2000
Connection ~ 4750 1900
Wire Wire Line
	4750 1600 4750 1900
Wire Wire Line
	4750 1150 6550 1150
Connection ~ 4750 1150
Wire Wire Line
	4750 1300 4750 1150
Wire Wire Line
	2700 1150 4400 1150
Wire Wire Line
	4350 1700 4400 1700
Wire Wire Line
	4400 1300 4400 1150
Connection ~ 4400 1150
Wire Wire Line
	4400 1150 4750 1150
Wire Wire Line
	4400 1600 4400 1700
Connection ~ 4400 1700
Wire Wire Line
	4400 1700 4900 1700
Wire Wire Line
	5550 1800 5550 4600
$Comp
L Device:R R10
U 1 1 5CB833FF
P 5350 4600
F 0 "R10" V 5250 4550 50  0000 L CNN
F 1 "470" V 5150 4550 50  0000 L CNN
F 2 "" V 5280 4600 50  0001 C CNN
F 3 "~" H 5350 4600 50  0001 C CNN
	1    5350 4600
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5150 4600 5200 4600
Wire Wire Line
	5500 4600 5550 4600
Connection ~ 5550 4600
Wire Wire Line
	5550 4600 5550 5350
Wire Wire Line
	3950 4600 3900 4600
Wire Wire Line
	3900 4600 3900 4300
Connection ~ 3900 4300
Wire Wire Line
	3900 4300 4000 4300
Wire Notes Line
	1200 5300 7650 5300
Wire Notes Line
	7650 5300 7650 5650
Wire Notes Line
	7650 5650 1200 5650
Wire Notes Line
	1200 5650 1200 5300
Text Notes 3050 5400 0    50   ~ 0
USB PCB Connectors
Text Notes 5350 6100 0    50   ~ 0
C1 Tantal - astable multivibrator trigger\nC2 Ceramic Multilayer - oscillating circuit avoidance
Wire Wire Line
	1300 1150 1600 1150
Wire Wire Line
	1400 4300 1800 4300
Wire Wire Line
	1800 4150 1800 4300
Connection ~ 1800 4300
Wire Wire Line
	1800 4300 2500 4300
Wire Wire Line
	1800 3850 1800 3300
Wire Wire Line
	1800 2800 1800 2750
Wire Wire Line
	1800 2750 1750 2750
Wire Wire Line
	1600 2600 1600 2500
Wire Wire Line
	1600 2100 1600 2000
Wire Wire Line
	1600 1700 1600 1650
Wire Wire Line
	1600 1300 1600 1150
Connection ~ 1600 1150
Wire Wire Line
	1600 1150 2500 1150
Wire Wire Line
	2100 2250 2000 2250
Wire Wire Line
	2000 2250 2000 1650
Wire Wire Line
	2000 1650 1600 1650
Connection ~ 1600 1650
Wire Wire Line
	1600 1650 1600 1600
Wire Wire Line
	1800 3300 2000 3300
Wire Wire Line
	2000 3300 2000 2650
Connection ~ 1800 3300
Wire Wire Line
	1800 3300 1800 3200
Connection ~ 2000 2650
Text Notes 8300 6450 0    50   ~ 0
SW1\nRF\n---\noff\noff\non\non
Text Notes 8500 6450 0    50   ~ 0
|\n|\n|\n|\n|\n|\n|
Text Notes 8550 6450 0    50   ~ 0
SW2/3\nF\n-----\noff\non\noff\non
Text Notes 8850 6450 0    50   ~ 0
|\n|\n|\n|\n|\n|\n|
Text Notes 8900 6450 0    50   ~ 0
U2A\nIN\n----\n 1  1\n 1  0\n010 1\n010 0
Text Notes 9150 6450 0    50   ~ 0
|\n|\n|\n|\n|\n|\n|
Text Notes 9200 6450 0    50   ~ 0
U2A\nOUT\n---\n0\n1\n010\n1
Text Notes 9350 6450 0    50   ~ 0
|\n|\n|\n|\n|\n|\n|
Text Notes 9400 6450 0    50   ~ 0
U2B\nOUT\n---\n1\n0\n101\n0
Text Notes 9550 6450 0    50   ~ 0
|\n|\n|\n|\n|\n|\n|
Text Notes 9600 6450 0    50   ~ 0
U4E\nOUT\n--------\nhiZ\nGND\nhiZ/GND/hiZ\nGND
Text Notes 8300 5800 0    50   ~ 0
Truth Table Fire
Text Notes 10100 6450 0    50   ~ 0
|\n|\n|\n|\n|\n|\n|
Text Notes 10150 6450 0    50   ~ 0
Result\n\n------\nno fire\nfire\nrapid fire\nfire
Text Notes 8300 5600 0    50   ~ 0
\n------\nOn (s)\nOff (s)\nCycle (s)\nFreq (Hz)
Text Notes 8650 5600 0    50   ~ 0
|\n|\n|\n|\n|\n|
Text Notes 8700 5600 0    50   ~ 0
R3=0Ohm\n------\n0.386\n0.379\n0.769\n1.3
Text Notes 9050 5600 0    50   ~ 0
|\n|\n|\n|\n|\n|
Text Notes 9100 5600 0    50   ~ 0
R3=50KOhm\n--------\n 0.0395\n 0.0325\n 0.0724\n13.87
Text Notes 8300 5050 0    50   ~ 0
Rapid Fire Speed
$EndSCHEMATC
