EESchema Schematic File Version 4
LIBS:joyweiler3-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Joyweiler Electric Diagram"
Date "2019-01-21"
Rev "3.1"
Comp "https://gitlab.com/edge-records/joyweiler"
Comment1 "http://creativecommons.org/licenses/by-sa/4.0/"
Comment2 "Licenced under Creative Commons Attribution-ShareAlike 4.0 International License."
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L 74xx:74LS00 U2
U 1 1 5C0B11BA
P 5200 1450
F 0 "U2" H 5200 1775 50  0000 C CNN
F 1 "74LS00" H 5200 1684 50  0000 C CNN
F 2 "" H 5200 1450 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls00" H 5200 1450 50  0001 C CNN
	1    5200 1450
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS00 U2
U 2 1 5C0B140B
P 6050 1450
F 0 "U2" H 6050 1775 50  0000 C CNN
F 1 "74LS00" H 6050 1684 50  0000 C CNN
F 2 "" H 6050 1450 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74ls00" H 6050 1450 50  0001 C CNN
	2    6050 1450
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_SPST SW2
U 1 1 5C0B2C63
P 4000 1650
F 0 "SW2" H 3950 1850 50  0000 C CNN
F 1 "Fire" H 3950 1750 50  0000 C CNN
F 2 "" H 4000 1650 50  0001 C CNN
F 3 "" H 4000 1650 50  0001 C CNN
	1    4000 1650
	1    0    0    -1  
$EndComp
$Comp
L joyweiler:NE555 U1
U 1 1 5C0B3AC6
P 2600 2100
F 0 "U1" H 2900 2600 50  0000 L CNN
F 1 "NE555" H 2850 2500 50  0000 L CNN
F 2 "" H 2600 2100 50  0001 C CNN
F 3 "" H 2600 2100 50  0001 C CNN
	1    2600 2100
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 5C0B3F29
P 1600 1500
F 0 "R2" H 1400 1550 50  0000 L CNN
F 1 "4.7K" H 1350 1450 50  0000 L CNN
F 2 "" V 1530 1500 50  0001 C CNN
F 3 "~" H 1600 1500 50  0001 C CNN
	1    1600 1500
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 5C0B3F9A
P 1600 1100
F 0 "R1" H 1400 1150 50  0000 L CNN
F 1 "1K" H 1400 1050 50  0000 L CNN
F 2 "" V 1530 1100 50  0001 C CNN
F 3 "~" H 1600 1100 50  0001 C CNN
	1    1600 1100
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 5C0B4AAB
P 2700 3650
F 0 "C2" H 2815 3696 50  0000 L CNN
F 1 "100nF" H 2815 3605 50  0000 L CNN
F 2 "" H 2738 3500 50  0001 C CNN
F 3 "~" H 2700 3650 50  0001 C CNN
	1    2700 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	2700 2550 2700 3500
Wire Wire Line
	2100 2300 2000 2300
Wire Wire Line
	2100 2100 2000 2100
Wire Wire Line
	2000 2100 2000 2300
Wire Wire Line
	2500 1700 2500 800 
Connection ~ 2500 800 
Wire Wire Line
	2500 800  2700 800 
Wire Wire Line
	2700 1650 2700 800 
Connection ~ 2700 800 
$Comp
L Device:CP C1
U 1 1 5C16358F
P 1800 3650
F 0 "C1" H 1918 3696 50  0000 L CNN
F 1 "10uF" H 1918 3605 50  0000 L CNN
F 2 "" H 1838 3500 50  0001 C CNN
F 3 "~" H 1800 3650 50  0001 C CNN
	1    1800 3650
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_SPST SW3
U 1 1 5C217379
P 4000 2050
F 0 "SW3" H 3950 2250 50  0000 C CNN
F 1 "Fire" H 3950 2150 50  0000 C CNN
F 2 "" H 4000 2050 50  0001 C CNN
F 3 "" H 4000 2050 50  0001 C CNN
	1    4000 2050
	1    0    0    -1  
$EndComp
$Comp
L 74xx_IEEE:7407 U3
U 5 1 5C3BCB17
P 8750 1200
F 0 "U3" H 9100 1550 50  0000 C CNN
F 1 "74LS07" H 9150 1450 50  0000 C CNN
F 2 "" H 8750 1200 50  0001 C CNN
F 3 "" H 8750 1200 50  0001 C CNN
	5    8750 1200
	1    0    0    -1  
$EndComp
$Comp
L 74xx_IEEE:7407 U3
U 3 1 5C3BCC96
P 8750 1900
F 0 "U3" H 9100 2250 50  0000 C CNN
F 1 "74LS07" H 9150 2150 50  0000 C CNN
F 2 "" H 8750 1900 50  0001 C CNN
F 3 "" H 8750 1900 50  0001 C CNN
	3    8750 1900
	1    0    0    -1  
$EndComp
$Comp
L 74xx_IEEE:7407 U3
U 2 1 5C71DB3A
P 8750 2550
F 0 "U3" H 9100 2900 50  0000 C CNN
F 1 "74LS07" H 9150 2800 50  0000 C CNN
F 2 "" H 8750 2550 50  0001 C CNN
F 3 "" H 8750 2550 50  0001 C CNN
	2    8750 2550
	1    0    0    -1  
$EndComp
$Comp
L 74xx_IEEE:7407 U3
U 1 1 5C7943B9
P 8750 3200
F 0 "U3" H 9100 3550 50  0000 C CNN
F 1 "74LS07" H 9150 3450 50  0000 C CNN
F 2 "" H 8750 3200 50  0001 C CNN
F 3 "" H 8750 3200 50  0001 C CNN
	1    8750 3200
	1    0    0    -1  
$EndComp
$Comp
L 74xx_IEEE:7407 U3
U 6 1 5C7EAEAB
P 8750 3850
F 0 "U3" H 9100 4200 50  0000 C CNN
F 1 "74LS07" H 9150 4100 50  0000 C CNN
F 2 "" H 8750 3850 50  0001 C CNN
F 3 "" H 8750 3850 50  0001 C CNN
	6    8750 3850
	1    0    0    -1  
$EndComp
$Comp
L Device:R_POT R3
U 1 1 5C2C47B3
P 1600 2400
F 0 "R3" H 1500 2500 50  0000 R CNN
F 1 "50K" H 1500 2400 50  0000 R CNN
F 2 "" H 1600 2400 50  0001 C CNN
F 3 "~" H 1600 2400 50  0001 C CNN
	1    1600 2400
	1    0    0    -1  
$EndComp
$Comp
L joyweiler:Conn_exploded-Connector_generic J1_1
U 1 1 5C349B20
P 1800 2650
F 0 "J1_1" V 1850 2450 50  0000 L CNN
F 1 "Pin" V 1845 2698 50  0001 L CNN
F 2 "" H 1800 2650 50  0001 C CNN
F 3 "~" H 1800 2650 50  0001 C CNN
	1    1800 2650
	0    1    1    0   
$EndComp
$Comp
L joyweiler:Conn_exploded-Connector_generic J1_2
U 1 1 5C3DF889
P 1600 1950
F 0 "J1_2" V 1650 1750 50  0000 L CNN
F 1 "Pin" V 1645 1998 50  0001 L CNN
F 2 "" H 1600 1950 50  0001 C CNN
F 3 "~" H 1600 1950 50  0001 C CNN
	1    1600 1950
	0    1    1    0   
$EndComp
Wire Wire Line
	2700 3800 2700 3950
Wire Wire Line
	2700 3950 2500 3950
Connection ~ 2500 3950
Wire Wire Line
	2500 2500 2500 3950
$Comp
L joyweiler:Conn_exploded-Connector_generic J1_4
U 1 1 5C5D5378
P 3250 1800
F 0 "J1_4" V 3250 1848 50  0000 L CNN
F 1 "Pin" V 3295 1848 50  0001 L CNN
F 2 "" H 3250 1800 50  0001 C CNN
F 3 "~" H 3250 1800 50  0001 C CNN
	1    3250 1800
	0    1    1    0   
$EndComp
$Comp
L Switch:SW_SPST SW1
U 1 1 5C5DB503
P 3650 1350
F 0 "SW1" H 3650 1585 50  0000 C CNN
F 1 "Rapid Fire" H 3650 1494 50  0000 C CNN
F 2 "" H 3650 1350 50  0001 C CNN
F 3 "" H 3650 1350 50  0001 C CNN
	1    3650 1350
	1    0    0    -1  
$EndComp
$Comp
L joyweiler:Conn_exploded-Connector_generic J1_6
U 1 1 5C5DE5A0
P 4150 1350
F 0 "J1_6" H 4150 1217 50  0000 C CNN
F 1 "Pin" H 4150 1216 50  0001 C CNN
F 2 "" H 4150 1350 50  0001 C CNN
F 3 "~" H 4150 1350 50  0001 C CNN
	1    4150 1350
	-1   0    0    1   
$EndComp
Wire Wire Line
	3850 1350 3950 1350
$Comp
L Device:R R4
U 1 1 5C5EAE2D
P 4400 1100
F 0 "R4" H 4470 1146 50  0000 L CNN
F 1 "100K" H 4470 1055 50  0000 L CNN
F 2 "" V 4330 1100 50  0001 C CNN
F 3 "~" H 4400 1100 50  0001 C CNN
	1    4400 1100
	1    0    0    -1  
$EndComp
$Comp
L Device:R R5
U 1 1 5C5EAF31
P 4750 1100
F 0 "R5" H 4820 1146 50  0000 L CNN
F 1 "100K" H 4820 1055 50  0000 L CNN
F 2 "" V 4680 1100 50  0001 C CNN
F 3 "~" H 4750 1100 50  0001 C CNN
	1    4750 1100
	1    0    0    -1  
$EndComp
Wire Wire Line
	5650 1450 5650 1350
Wire Wire Line
	5650 1350 5750 1350
Wire Wire Line
	5750 1550 5650 1550
Wire Wire Line
	5650 1550 5650 1450
Connection ~ 5650 1450
$Comp
L joyweiler:Conn_exploded-Connector_generic J2_1
U 1 1 5C61DDAA
P 3500 2400
F 0 "J2_1" V 3500 2448 50  0000 L CNN
F 1 "Pin" V 3545 2448 50  0001 L CNN
F 2 "" H 3500 2400 50  0001 C CNN
F 3 "~" H 3500 2400 50  0001 C CNN
	1    3500 2400
	0    1    1    0   
$EndComp
$Comp
L joyweiler:Conn_exploded-Connector_generic J3_1
U 1 1 5C61DED7
P 3750 2400
F 0 "J3_1" V 3750 2448 50  0000 L CNN
F 1 "Pin" V 3795 2448 50  0001 L CNN
F 2 "" H 3750 2400 50  0001 C CNN
F 3 "~" H 3750 2400 50  0001 C CNN
	1    3750 2400
	0    1    1    0   
$EndComp
$Comp
L joyweiler:Conn_exploded-Connector_generic J2_2
U 1 1 5C621C37
P 4450 1650
F 0 "J2_2" H 4450 1550 50  0000 C CNN
F 1 "Pin" H 4450 1600 50  0001 C CNN
F 2 "" H 4450 1650 50  0001 C CNN
F 3 "~" H 4450 1650 50  0001 C CNN
	1    4450 1650
	-1   0    0    1   
$EndComp
$Comp
L joyweiler:Conn_exploded-Connector_generic J3_2
U 1 1 5C621CD0
P 4450 2050
F 0 "J3_2" H 4450 1950 50  0000 C CNN
F 1 "Pin" H 4450 2000 50  0001 C CNN
F 2 "" H 4450 2050 50  0001 C CNN
F 3 "~" H 4450 2050 50  0001 C CNN
	1    4450 2050
	-1   0    0    1   
$EndComp
Wire Wire Line
	3500 2200 3500 1650
Wire Wire Line
	3500 1650 3800 1650
Wire Wire Line
	3750 2200 3750 2050
Wire Wire Line
	3750 2050 3800 2050
Wire Wire Line
	4200 1650 4250 1650
Wire Wire Line
	4200 2050 4250 2050
Wire Wire Line
	3100 2100 3250 2100
Wire Wire Line
	3250 2100 3250 2000
Wire Wire Line
	3250 1600 3250 1350
Wire Wire Line
	3250 1350 3450 1350
Wire Wire Line
	3500 2600 3500 3950
Wire Wire Line
	3500 3950 2700 3950
Connection ~ 2700 3950
Wire Wire Line
	3750 2600 3750 3950
Wire Wire Line
	3750 3950 3500 3950
Connection ~ 3500 3950
$Comp
L joyweiler:Joystick-joystick SW4
U 1 1 5C68BB63
P 4200 2700
F 0 "SW4" H 4240 2948 50  0000 C CNN
F 1 "Joystick" H 4240 2857 50  0000 C CNN
F 2 "" H 4200 2700 50  0001 C CNN
F 3 "" H 4200 2700 50  0001 C CNN
	1    4200 2700
	1    0    0    -1  
$EndComp
Connection ~ 3750 3950
Wire Wire Line
	5150 2700 5150 1900
Wire Wire Line
	5350 2900 5350 2550
Wire Wire Line
	5350 3100 5350 3200
Wire Wire Line
	5150 3300 5150 3850
Wire Wire Line
	5150 3850 5950 3850
$Comp
L Device:R R6
U 1 1 5C6ADE93
P 6550 1100
F 0 "R6" H 6620 1146 50  0000 L CNN
F 1 "22K" H 6620 1055 50  0000 L CNN
F 2 "" V 6480 1100 50  0001 C CNN
F 3 "~" H 6550 1100 50  0001 C CNN
	1    6550 1100
	1    0    0    -1  
$EndComp
$Comp
L Device:R R7
U 1 1 5C6ADFA7
P 6850 1100
F 0 "R7" H 6920 1146 50  0000 L CNN
F 1 "22K" H 6920 1055 50  0000 L CNN
F 2 "" V 6780 1100 50  0001 C CNN
F 3 "~" H 6850 1100 50  0001 C CNN
	1    6850 1100
	1    0    0    -1  
$EndComp
$Comp
L Device:R R8
U 1 1 5C6AE054
P 7150 1100
F 0 "R8" H 7220 1146 50  0000 L CNN
F 1 "22K" H 7220 1055 50  0000 L CNN
F 2 "" V 7080 1100 50  0001 C CNN
F 3 "~" H 7150 1100 50  0001 C CNN
	1    7150 1100
	1    0    0    -1  
$EndComp
$Comp
L Device:R R9
U 1 1 5C6AE10E
P 7450 1100
F 0 "R9" H 7520 1146 50  0000 L CNN
F 1 "22K" H 7520 1055 50  0000 L CNN
F 2 "" V 7380 1100 50  0001 C CNN
F 3 "~" H 7450 1100 50  0001 C CNN
	1    7450 1100
	1    0    0    -1  
$EndComp
Wire Wire Line
	6550 950  6550 800 
Connection ~ 6550 800 
Wire Wire Line
	6550 800  6850 800 
Wire Wire Line
	6850 950  6850 800 
Connection ~ 6850 800 
Wire Wire Line
	6850 800  7150 800 
Wire Wire Line
	7150 950  7150 800 
Connection ~ 7150 800 
Wire Wire Line
	7150 800  7450 800 
Wire Wire Line
	7450 950  7450 800 
Wire Wire Line
	6550 1250 6550 1900
Connection ~ 6550 1900
Wire Wire Line
	6550 1900 8200 1900
Wire Wire Line
	6850 1250 6850 2550
Connection ~ 6850 2550
Wire Wire Line
	6850 2550 8200 2550
Wire Wire Line
	7150 1250 7150 3200
Connection ~ 7150 3200
Wire Wire Line
	7150 3200 8200 3200
Wire Wire Line
	7450 1250 7450 3850
Connection ~ 7450 3850
Wire Wire Line
	6350 1450 8050 1450
Wire Wire Line
	8050 1450 8050 1200
Wire Wire Line
	8050 1200 8200 1200
$Comp
L joyweiler:Conn_exploded-Connector_generic J5_1
U 1 1 5C791F3B
P 4000 3650
F 0 "J5_1" V 4000 3698 50  0000 L CNN
F 1 "Pin" V 4045 3698 50  0001 L CNN
F 2 "" H 4000 3650 50  0001 C CNN
F 3 "~" H 4000 3650 50  0001 C CNN
	1    4000 3650
	0    1    1    0   
$EndComp
$Comp
L joyweiler:Conn_exploded-Connector_generic J5_2
U 1 1 5C792062
P 4800 2700
F 0 "J5_2" H 4800 2650 50  0000 C CNN
F 1 "Pin" H 4800 2650 50  0001 C CNN
F 2 "" H 4800 2700 50  0001 C CNN
F 3 "~" H 4800 2700 50  0001 C CNN
	1    4800 2700
	-1   0    0    1   
$EndComp
$Comp
L joyweiler:Conn_exploded-Connector_generic J5_3
U 1 1 5C79226F
P 4800 2900
F 0 "J5_3" H 4800 2850 50  0000 C CNN
F 1 "Pin" H 4800 2850 50  0001 C CNN
F 2 "" H 4800 2900 50  0001 C CNN
F 3 "~" H 4800 2900 50  0001 C CNN
	1    4800 2900
	-1   0    0    1   
$EndComp
$Comp
L joyweiler:Conn_exploded-Connector_generic J5_4
U 1 1 5C792326
P 4800 3100
F 0 "J5_4" H 4800 3050 50  0000 C CNN
F 1 "Pin" H 4800 3050 50  0001 C CNN
F 2 "" H 4800 3100 50  0001 C CNN
F 3 "~" H 4800 3100 50  0001 C CNN
	1    4800 3100
	-1   0    0    1   
$EndComp
$Comp
L joyweiler:Conn_exploded-Connector_generic J5_5
U 1 1 5C7923DE
P 4800 3300
F 0 "J5_5" H 4800 3250 50  0000 C CNN
F 1 "Pin" H 4800 3250 50  0001 C CNN
F 2 "" H 4800 3300 50  0001 C CNN
F 3 "~" H 4800 3300 50  0001 C CNN
	1    4800 3300
	-1   0    0    1   
$EndComp
Wire Wire Line
	4000 3850 4000 3950
Wire Wire Line
	4000 3300 4000 3450
Wire Wire Line
	4400 3300 4600 3300
Wire Wire Line
	4400 3100 4600 3100
Wire Wire Line
	4400 2900 4600 2900
Wire Wire Line
	4400 2700 4600 2700
Wire Wire Line
	5000 2700 5150 2700
Wire Wire Line
	5000 2900 5350 2900
Wire Wire Line
	5000 3100 5350 3100
Wire Wire Line
	5000 3300 5150 3300
$Comp
L Device:LED D2
U 1 1 5C82263E
P 4750 3950
F 0 "D2" H 4750 4050 50  0000 C CNN
F 1 "LED" H 4750 4150 50  0000 C CNN
F 2 "" H 4750 3950 50  0001 C CNN
F 3 "~" H 4750 3950 50  0001 C CNN
	1    4750 3950
	1    0    0    1   
$EndComp
$Comp
L joyweiler:Conn_exploded-Connector_generic J1_3
U 1 1 5C822772
P 5150 3950
F 0 "J1_3" H 5150 4050 50  0000 C CNN
F 1 "Pin" H 5150 3816 50  0001 C CNN
F 2 "" H 5150 3950 50  0001 C CNN
F 3 "~" H 5150 3950 50  0001 C CNN
	1    5150 3950
	-1   0    0    1   
$EndComp
$Comp
L joyweiler:Conn_exploded-Connector_generic J1_5
U 1 1 5C822877
P 4350 3950
F 0 "J1_5" H 4350 4050 50  0000 C CNN
F 1 "Pin" H 4350 3816 50  0001 C CNN
F 2 "" H 4350 3950 50  0001 C CNN
F 3 "~" H 4350 3950 50  0001 C CNN
	1    4350 3950
	-1   0    0    1   
$EndComp
Wire Wire Line
	4550 3950 4600 3950
Wire Wire Line
	4900 3950 4950 3950
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J4
U 1 1 5C86713C
P 10050 2550
F 0 "J4" H 10100 2250 50  0000 C CNN
F 1 "C64" H 10100 2150 50  0000 C CNN
F 2 "" H 10050 2550 50  0001 C CNN
F 3 "~" H 10050 2550 50  0001 C CNN
	1    10050 2550
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x03_Odd_Even J1
U 1 1 5C86D924
P 1900 6600
F 0 "J1" H 1950 6917 50  0000 C CNN
F 1 "Rapid Fire" H 1950 6850 50  0000 C CNN
F 2 "" H 1900 6600 50  0001 C CNN
F 3 "~" H 1900 6600 50  0001 C CNN
	1    1900 6600
	1    0    0    -1  
$EndComp
Wire Wire Line
	9400 1900 9400 2450
Wire Wire Line
	9400 2450 9850 2450
Wire Wire Line
	9300 1900 9400 1900
Wire Wire Line
	9300 3200 10650 3200
Wire Wire Line
	10650 3200 10650 2350
Wire Wire Line
	10650 2350 10350 2350
Wire Wire Line
	9300 2550 9400 2550
Wire Wire Line
	9400 2550 9400 3100
Wire Wire Line
	9400 3100 10550 3100
Wire Wire Line
	10550 3100 10550 2450
Wire Wire Line
	10550 2450 10350 2450
Wire Wire Line
	9300 3850 9600 3850
Wire Wire Line
	9600 3850 9600 2350
Wire Wire Line
	9600 2350 9850 2350
Wire Wire Line
	10750 2550 10350 2550
Wire Wire Line
	9850 2650 9500 2650
Wire Wire Line
	9500 2650 9500 800 
$Comp
L Diode:1N5817 D1
U 1 1 5C8D8617
P 8050 800
F 0 "D1" H 8050 1016 50  0000 C CNN
F 1 "1N5817" H 8050 925 50  0000 C CNN
F 2 "Diode_THT:D_DO-41_SOD81_P10.16mm_Horizontal" H 8050 625 50  0001 C CNN
F 3 "http://www.vishay.com/docs/88525/1n5817.pdf" H 8050 800 50  0001 C CNN
	1    8050 800 
	1    0    0    -1  
$EndComp
Wire Wire Line
	7900 800  7750 800 
Connection ~ 7450 800 
Wire Wire Line
	8200 800  9500 800 
NoConn ~ 9850 2550
NoConn ~ 9850 2750
NoConn ~ 10350 2750
Wire Wire Line
	10350 2650 10750 2650
Wire Wire Line
	1400 6500 1700 6500
Wire Wire Line
	1400 6600 1700 6600
Wire Wire Line
	1400 6700 1700 6700
Wire Wire Line
	2550 6500 2200 6500
Wire Wire Line
	2550 6600 2200 6600
Wire Wire Line
	2550 6700 2200 6700
Text Label 2250 6500 0    50   ~ 0
Potentiometer_Fire_Speed_IN
Text Label 650  6500 0    50   ~ 0
Potentiometer_Fire_Speed_OUT
Text Label 1450 6600 0    50   ~ 0
LED_IN
Text Label 1400 6700 0    50   ~ 0
LED_OUT
Text Label 2250 6600 0    50   ~ 0
Rapid_Fire_Toggle_Switch_IN
Text Label 2250 6700 0    50   ~ 0
Rapid_Fire_Toggle_Switch_OUT
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J4
U 1 1 5CA2E54E
P 2650 7200
F 0 "J4" H 2700 6900 50  0000 C CNN
F 1 "C64" H 2700 6800 50  0000 C CNN
F 2 "" H 2650 7200 50  0001 C CNN
F 3 "~" H 2650 7200 50  0001 C CNN
	1    2650 7200
	1    0    0    -1  
$EndComp
Wire Wire Line
	2150 7000 2450 7000
Wire Wire Line
	2150 7100 2450 7100
Wire Wire Line
	2150 7200 2450 7200
Wire Wire Line
	2150 7300 2450 7300
Wire Wire Line
	2150 7400 2450 7400
Wire Wire Line
	3300 7000 2950 7000
Wire Wire Line
	3300 7100 2950 7100
Wire Wire Line
	3300 7200 2950 7200
Wire Wire Line
	3300 7300 2950 7300
Wire Wire Line
	3300 7400 2950 7400
Text Label 2250 7000 0    50   ~ 0
Up
Text Label 2250 7100 0    50   ~ 0
Left
Text Label 2250 7300 0    50   ~ 0
+5V
Text Label 3000 7000 0    50   ~ 0
Down
Text Label 3000 7100 0    50   ~ 0
Right
Text Label 3000 7200 0    50   ~ 0
Fire
Text Label 3000 7300 0    50   ~ 0
GND
NoConn ~ 2150 7200
NoConn ~ 2150 7400
NoConn ~ 3300 7400
Wire Notes Line
	3450 6200 600  6200
Text Notes 2450 6300 0    50   ~ 0
Ribbon Cable Connectors
Wire Notes Line
	3450 6200 3450 7700
Wire Notes Line
	3450 7700 600  7700
Wire Notes Line
	600  7700 600  6200
Text Notes 5950 7350 0    50   ~ 0
J1 Rapid Fire\nJ2 Fire1\nJ3 Fire2\nJ4 C64\nJ5 Sanwa Joystick\nJ6 USB\nJ7 SPI Programming
Text Notes 3500 6600 0    50   ~ 0
U1 NE555 (Timer)\nU2 74LS00 (NAND)\nU3 74LS07 (Open Collector)\nU4 ATtiny84A (USB Driver)
Text Notes 4850 7350 0    50   ~ 0
SW1 Rapid Fire Toggle\nSW2 Fire1\nSW3 Fire2\nSW4 Sanwa Joystick
Text Notes 4850 7000 0    50   ~ 0
D1 C64 Power\nD2 Fire LED\nD3 Z-Diode 3.6V\nD4 Z-Diode 3.6V
Text Notes 3500 7700 0    50   ~ 0
R1 Timer\nR2 Timer\nR3 Potentiometer Fire Speed\nR4 Pull-up Rapid Fire\nR5 Pull-up Manual Fire\nR6 Pull-up Left\nR7 Pull-up Right\nR8 Pull-up Down\nR9 Pull-up Up\nR10 LED Series Resistor\nR11 USB Series Resistor\nR12 USB Series Resistor\nR13 USB Low Speed D- Pull-Up
Wire Wire Line
	4650 1650 4750 1650
Wire Wire Line
	4650 2050 4750 2050
Wire Wire Line
	4750 1550 4900 1550
Wire Wire Line
	4750 1650 4750 1550
Connection ~ 4750 1650
Wire Wire Line
	4750 2050 4750 1650
Connection ~ 4750 1550
Wire Wire Line
	4750 1250 4750 1550
Wire Wire Line
	4750 800  6550 800 
Connection ~ 4750 800 
Wire Wire Line
	4750 950  4750 800 
Wire Wire Line
	2700 800  4400 800 
Wire Wire Line
	4350 1350 4400 1350
Wire Wire Line
	4400 950  4400 800 
Connection ~ 4400 800 
Wire Wire Line
	4400 800  4750 800 
Wire Wire Line
	4400 1250 4400 1350
Connection ~ 4400 1350
Wire Wire Line
	4400 1350 4900 1350
$Comp
L Device:R R10
U 1 1 5CB833FF
P 5550 3950
F 0 "R10" V 5450 3900 50  0000 L CNN
F 1 "470R" V 5350 3850 50  0000 L CNN
F 2 "" V 5480 3950 50  0001 C CNN
F 3 "~" H 5550 3950 50  0001 C CNN
	1    5550 3950
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5350 3950 5400 3950
Text Notes 4850 7700 0    50   ~ 0
C1 Tantal - astable multivibrator trigger\nC2 Ceramic Multilayer - oscillating circuit avoidance\nC3 Tantal - USB Protection\nC4 Ceramic - IC protection
Wire Wire Line
	1800 3800 1800 3950
Wire Wire Line
	1800 3950 2500 3950
Wire Wire Line
	1800 3500 1800 2950
Wire Wire Line
	1800 2450 1800 2400
Wire Wire Line
	1800 2400 1750 2400
Wire Wire Line
	1600 2250 1600 2150
Wire Wire Line
	1600 1750 1600 1650
Wire Wire Line
	1600 1350 1600 1300
Wire Wire Line
	1600 950  1600 800 
Connection ~ 1600 800 
Wire Wire Line
	1600 800  2500 800 
Wire Wire Line
	2100 1900 2000 1900
Wire Wire Line
	2000 1900 2000 1300
Wire Wire Line
	2000 1300 1600 1300
Connection ~ 1600 1300
Wire Wire Line
	1600 1300 1600 1250
Wire Wire Line
	1800 2950 2000 2950
Wire Wire Line
	2000 2950 2000 2300
Connection ~ 1800 2950
Wire Wire Line
	1800 2950 1800 2850
Connection ~ 2000 2300
Text Notes 8350 4900 0    50   ~ 0
SW1\nRF\n---\noff\noff\non\non
Text Notes 8550 4900 0    50   ~ 0
|\n|\n|\n|\n|\n|\n|
Text Notes 8600 4900 0    50   ~ 0
SW2/3\nF\n-----\noff\non\noff\non
Text Notes 8900 4900 0    50   ~ 0
|\n|\n|\n|\n|\n|\n|
Text Notes 8950 4900 0    50   ~ 0
U2A\nIN\n----\n 1  1\n 1  0\n010 1\n010 0
Text Notes 9200 4900 0    50   ~ 0
|\n|\n|\n|\n|\n|\n|
Text Notes 9250 4900 0    50   ~ 0
U2A\nOUT\n---\n0\n1\n010\n1
Text Notes 9400 4900 0    50   ~ 0
|\n|\n|\n|\n|\n|\n|
Text Notes 9450 4900 0    50   ~ 0
U2B\nOUT\n---\n1\n0\n101\n0
Text Notes 9600 4900 0    50   ~ 0
|\n|\n|\n|\n|\n|\n|
Text Notes 9650 4900 0    50   ~ 0
U3E\nOUT\n--------\nhiZ\nGND\nhiZ/GND/hiZ\nGND
Text Notes 8050 4600 0    50   ~ 0
Truth-\nTable\nFire
Text Notes 10150 4900 0    50   ~ 0
|\n|\n|\n|\n|\n|\n|
Text Notes 10200 4900 0    50   ~ 0
Result\n\n------\nno fire\nfire\nrapid fire\nfire
Text Notes 9800 1400 0    50   ~ 0
\n------\nOn (s)\nOff (s)\nCycle (s)\nFreq (Hz)
Text Notes 10150 1400 0    50   ~ 0
|\n|\n|\n|\n|\n|
Text Notes 10200 1400 0    50   ~ 0
R3=0Ohm\n------\n0.386\n0.379\n0.769\n1.3
Text Notes 10550 1400 0    50   ~ 0
|\n|\n|\n|\n|\n|
Text Notes 10600 1400 0    50   ~ 0
R3=50KOhm\n--------\n 0.0395\n 0.0325\n 0.0724\n13.87
Text Notes 9750 1000 0    50   ~ 0
Rapid Fire\nSpeed
$Comp
L MCU_Microchip_ATtiny:ATtiny84A-PU U4
U 1 1 5C498D64
P 3000 5100
F 0 "U4" H 2470 5146 50  0000 R CNN
F 1 "ATtiny84A-PU" H 2470 5055 50  0000 R CNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 3000 5100 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/doc8183.pdf" H 3000 5100 50  0001 C CNN
	1    3000 5100
	1    0    0    -1  
$EndComp
Wire Wire Line
	5150 1900 6400 1900
Wire Wire Line
	5350 2550 6250 2550
Wire Wire Line
	5350 3200 6100 3200
Wire Wire Line
	7450 3850 8200 3850
Wire Wire Line
	5950 3850 5950 4500
Wire Wire Line
	5950 4500 3600 4500
Connection ~ 5950 3850
Wire Wire Line
	5950 3850 7450 3850
Wire Wire Line
	6100 3200 6100 4600
Wire Wire Line
	6100 4600 3600 4600
Connection ~ 6100 3200
Wire Wire Line
	6100 3200 7150 3200
Wire Wire Line
	6250 2550 6250 4700
Wire Wire Line
	6250 4700 3600 4700
Connection ~ 6250 2550
Wire Wire Line
	6250 2550 6850 2550
Wire Wire Line
	6400 1900 6400 4800
Wire Wire Line
	6400 4800 3600 4800
Connection ~ 6400 1900
Wire Wire Line
	6400 1900 6550 1900
Wire Wire Line
	3000 4200 3000 4100
Wire Wire Line
	1300 4100 1300 800 
Wire Wire Line
	1300 800  1600 800 
Wire Wire Line
	3000 6050 3000 6000
Connection ~ 4000 3950
$Comp
L Device:C C4
U 1 1 5C5888BE
P 1300 5150
F 0 "C4" H 1050 5200 50  0000 L CNN
F 1 "100nF" H 950 5100 50  0000 L CNN
F 2 "" H 1338 5000 50  0001 C CNN
F 3 "~" H 1300 5150 50  0001 C CNN
	1    1300 5150
	1    0    0    -1  
$EndComp
Connection ~ 3000 6050
$Comp
L Connector_Generic:Conn_01x04 J6
U 1 1 5C59A52D
P 10150 5500
F 0 "J6" H 10229 5492 50  0000 L CNN
F 1 "USB" H 10229 5401 50  0000 L CNN
F 2 "" H 10150 5500 50  0001 C CNN
F 3 "~" H 10150 5500 50  0001 C CNN
	1    10150 5500
	1    0    0    -1  
$EndComp
Text Label 9750 5400 0    50   ~ 0
VCC
Text Label 9800 5500 0    50   ~ 0
D-
Text Label 9800 5600 0    50   ~ 0
D+
Text Label 9750 5700 0    50   ~ 0
GND
Wire Wire Line
	10750 2650 10750 6050
Wire Wire Line
	9500 6050 9500 5700
Wire Wire Line
	9500 5700 9950 5700
Wire Wire Line
	9500 6050 10750 6050
Connection ~ 9500 6050
Wire Wire Line
	5500 1450 5550 1450
Wire Wire Line
	3750 3950 4000 3950
Wire Wire Line
	5800 1750 5550 1750
Wire Wire Line
	5550 1750 5550 1450
Wire Wire Line
	5700 3950 5800 3950
Connection ~ 5550 1450
Wire Wire Line
	5550 1450 5650 1450
Wire Wire Line
	5800 3950 5800 1750
Wire Wire Line
	4150 3950 4000 3950
Wire Wire Line
	7750 5400 7750 4700
Wire Wire Line
	7750 5400 8900 5400
Connection ~ 7750 800 
Wire Wire Line
	7750 800  7450 800 
$Comp
L Device:CP C3
U 1 1 5C6B0563
P 8900 5850
F 0 "C3" H 9018 5896 50  0000 L CNN
F 1 "4.7uF" H 9018 5805 50  0000 L CNN
F 2 "" H 8938 5700 50  0001 C CNN
F 3 "~" H 8900 5850 50  0001 C CNN
	1    8900 5850
	1    0    0    -1  
$EndComp
Wire Wire Line
	8900 5700 8900 5400
Connection ~ 8900 5400
Wire Wire Line
	8900 5400 9950 5400
Wire Wire Line
	8900 6000 8900 6050
Connection ~ 8900 6050
Wire Wire Line
	8900 6050 9500 6050
$Comp
L Device:D_Zener D3
U 1 1 5C6C3973
P 5950 5850
F 0 "D3" V 5904 5929 50  0000 L CNN
F 1 "3.6V" V 5995 5929 50  0000 L CNN
F 2 "" H 5950 5850 50  0001 C CNN
F 3 "~" H 5950 5850 50  0001 C CNN
	1    5950 5850
	0    1    1    0   
$EndComp
Wire Wire Line
	5800 5200 5800 3950
Wire Wire Line
	3600 5200 5800 5200
Connection ~ 5800 3950
$Comp
L Device:D_Zener D4
U 1 1 5C6CD9D9
P 6450 5850
F 0 "D4" V 6404 5929 50  0000 L CNN
F 1 "3.6V" V 6495 5929 50  0000 L CNN
F 2 "" H 6450 5850 50  0001 C CNN
F 3 "~" H 6450 5850 50  0001 C CNN
	1    6450 5850
	0    1    1    0   
$EndComp
Wire Wire Line
	5950 6000 5950 6050
Connection ~ 5950 6050
Wire Wire Line
	5950 6050 6450 6050
Wire Wire Line
	6450 6000 6450 6050
Connection ~ 6450 6050
Wire Wire Line
	6450 6050 8900 6050
$Comp
L Device:R R12
U 1 1 5C6E1B58
P 5100 5700
F 0 "R12" V 5250 5550 50  0000 L CNN
F 1 "68R" V 5250 5750 50  0000 L CNN
F 2 "" V 5030 5700 50  0001 C CNN
F 3 "~" H 5100 5700 50  0001 C CNN
	1    5100 5700
	0    1    1    0   
$EndComp
$Comp
L Device:R R11
U 1 1 5C6E22B3
P 5100 5400
F 0 "R11" V 5000 5250 50  0000 L CNN
F 1 "68R" V 5000 5450 50  0000 L CNN
F 2 "" V 5030 5400 50  0001 C CNN
F 3 "~" H 5100 5400 50  0001 C CNN
	1    5100 5400
	0    1    1    0   
$EndComp
Wire Wire Line
	6450 5700 6450 5600
Wire Wire Line
	6450 5600 9950 5600
Wire Wire Line
	3600 5600 4850 5600
Wire Wire Line
	4850 5600 4850 5700
Wire Wire Line
	4850 5700 4950 5700
Wire Wire Line
	5250 5700 5350 5700
Wire Wire Line
	5350 5700 5350 5600
Wire Wire Line
	5350 5600 6450 5600
Connection ~ 6450 5600
Wire Wire Line
	3600 5500 4850 5500
Wire Wire Line
	4850 5500 4850 5400
Wire Wire Line
	4850 5400 4950 5400
Wire Wire Line
	5250 5400 5350 5400
Wire Wire Line
	5350 5400 5350 5500
Wire Wire Line
	5350 5500 5950 5500
Wire Wire Line
	5950 5700 5950 5500
Connection ~ 5950 5500
$Comp
L Device:R R13
U 1 1 5C74DFFC
P 7400 5050
F 0 "R13" H 7200 5100 50  0000 L CNN
F 1 "1.5K" H 7150 5000 50  0000 L CNN
F 2 "" V 7330 5050 50  0001 C CNN
F 3 "~" H 7400 5050 50  0001 C CNN
	1    7400 5050
	1    0    0    -1  
$EndComp
Wire Wire Line
	7400 5200 7400 5500
Wire Wire Line
	5950 5500 7400 5500
Connection ~ 7400 5500
Wire Wire Line
	7400 5500 9950 5500
Wire Wire Line
	7400 4900 7400 4700
Wire Wire Line
	7400 4700 7750 4700
Connection ~ 7750 4700
Wire Wire Line
	7750 4700 7750 800 
$Comp
L Connector_Generic:Conn_02x03_Odd_Even J7
U 1 1 5C772DC9
P 1250 7300
F 0 "J7" H 1300 7100 50  0000 C CNN
F 1 "SPI" H 1300 7000 50  0000 C CNN
F 2 "" H 1250 7300 50  0001 C CNN
F 3 "~" H 1250 7300 50  0001 C CNN
	1    1250 7300
	1    0    0    -1  
$EndComp
Wire Wire Line
	750  7200 1050 7200
Wire Wire Line
	750  7300 1050 7300
Wire Wire Line
	750  7400 1050 7400
Wire Wire Line
	1850 7200 1550 7200
Wire Wire Line
	1850 7300 1550 7300
Wire Wire Line
	1850 7400 1550 7400
Text Label 800  7200 0    50   ~ 0
MISO
Text Label 800  7300 0    50   ~ 0
SCK
Text Label 800  7400 0    50   ~ 0
Reset
Text Label 1600 7300 0    50   ~ 0
MOSI
Text Label 1600 7400 0    50   ~ 0
GND
$Comp
L joyweiler:Conn_01x01_Male-Connector_single_generic J7_5_Reset
U 1 1 5C828935
P 4300 5700
F 0 "J7_5_Reset" H 4050 5700 50  0000 C CNN
F 1 "Conn_01x01_Male" H 4406 5787 50  0001 C CNN
F 2 "" H 4300 5700 50  0001 C CNN
F 3 "~" H 4300 5700 50  0001 C CNN
	1    4300 5700
	-1   0    0    -1  
$EndComp
$Comp
L joyweiler:Conn_01x01_Male-Connector_single_generic J7_3_SCL
U 1 1 5C843332
P 4300 4900
F 0 "J7_3_SCL" H 4100 4900 50  0000 C CNN
F 1 "Conn_01x01_Male" H 4406 4987 50  0001 C CNN
F 2 "" H 4300 4900 50  0001 C CNN
F 3 "~" H 4300 4900 50  0001 C CNN
	1    4300 4900
	-1   0    0    -1  
$EndComp
$Comp
L joyweiler:Conn_01x01_Male-Connector_single_generic J7_1_MISO
U 1 1 5C84344E
P 4300 5000
F 0 "J7_1_MISO" H 4100 5000 50  0000 C CNN
F 1 "Conn_01x01_Male" H 4406 5087 50  0001 C CNN
F 2 "" H 4300 5000 50  0001 C CNN
F 3 "~" H 4300 5000 50  0001 C CNN
	1    4300 5000
	-1   0    0    -1  
$EndComp
$Comp
L joyweiler:Conn_01x01_Male-Connector_single_generic J7_4_MOSI
U 1 1 5C843549
P 4300 5100
F 0 "J7_4_MOSI" H 4100 5100 50  0000 C CNN
F 1 "Conn_01x01_Male" H 4406 5187 50  0001 C CNN
F 2 "" H 4300 5100 50  0001 C CNN
F 3 "~" H 4300 5100 50  0001 C CNN
	1    4300 5100
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3600 4900 4100 4900
Wire Wire Line
	3600 5000 4100 5000
Wire Wire Line
	3600 5100 4100 5100
Wire Wire Line
	3600 5700 4100 5700
Wire Wire Line
	3000 6050 5950 6050
Wire Wire Line
	1300 4100 3000 4100
Wire Wire Line
	1300 6050 1300 5300
Wire Wire Line
	1300 6050 1800 6050
Wire Wire Line
	1300 5000 1300 4100
Connection ~ 1300 4100
Wire Wire Line
	1800 6050 1800 3950
Connection ~ 1800 6050
Wire Wire Line
	1800 6050 3000 6050
Connection ~ 1800 3950
Text Label 1900 3950 0    50   ~ 0
GND
Text Label 1750 800  0    50   ~ 0
+5V
Text Label 1900 6050 0    50   ~ 0
GND
NoConn ~ 1850 7200
Wire Notes Line
	8000 4300 10600 4300
Wire Notes Line
	10600 4300 10600 4950
Wire Notes Line
	10600 4950 8000 4950
Wire Notes Line
	8000 4950 8000 4300
Wire Wire Line
	10750 2550 10750 2200
Wire Wire Line
	10750 2200 9600 2200
Wire Wire Line
	9600 2200 9600 1200
Wire Wire Line
	9600 1200 9300 1200
Wire Notes Line
	9700 800  9700 1450
Wire Notes Line
	9700 1450 11150 1450
Wire Notes Line
	11150 1450 11150 800 
Wire Notes Line
	11150 800  9700 800 
$EndSCHEMATC
